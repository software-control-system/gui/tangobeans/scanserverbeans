package fr.soleil.bean.scanserver;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import fr.soleil.lib.project.ObjectUtils;

public class CurrentScan2DActuatorInput extends ACurrentScanActuatorInput implements ActionListener {

    private static final long serialVersionUID = 6027478499154840083L;

    private static final String SEND_LINE = "Send line";

    // index of line, [attributeNameX,attributeNameY]
    private final Map<String, String[]> dualMap;
    private final List<JButton> buttonList;

    public CurrentScan2DActuatorInput(boolean isSendSensor) {
        super(isSendSensor, false);
        dualMap = new HashMap<>();
        buttonList = new ArrayList<>();
        initGUI();
    }

    public CurrentScan2DActuatorInput() {
        this(DEFAULT_SEND_SENSOR);
    }

    public void setActuatorIndex(int xindex, int yindex) {
        for (SimpleActuatorAttributeInputBean input : inputMap.values()) {
            if (input != null) {
                input.setActuatorIndex(xindex, yindex);
            }
        }
    }

    private void removeListener() {
        for (JButton button : buttonList) {
            if (button != null) {
                button.removeActionListener(this);
            }
        }
    }

    @Override
    protected void clearGUI() {
        super.clearGUI();
        removeListener();
        buttonList.clear();
    }

    @Override
    protected void refreshGUI(String[] tmpXList, String[] tmpXDataList, String[] tmpXActuatorsDataList,
            String[] tmpXActuatorsList, String[] tmpYActuatorsDataList, String[] tmpYActuatorsList) {
        if ((tmpXActuatorsDataList != null) && (tmpXActuatorsDataList.length > 0) && (tmpXActuatorsList != null)
                && (tmpXActuatorsList.length > 0) && (tmpXActuatorsDataList.length == tmpXActuatorsList.length)
                && (tmpYActuatorsDataList != null) && (tmpYActuatorsDataList.length > 0) && (tmpYActuatorsList != null)
                && (tmpYActuatorsList.length > 0) && (tmpYActuatorsDataList.length == tmpYActuatorsList.length)
                && (tmpXActuatorsDataList.length == tmpYActuatorsDataList.length)) {
            GridBagConstraints constraints = null;
            SimpleActuatorAttributeInputBean input = null;
            JButton dualButton = null;
            String xactuatorAttributeName = null;
            String yactuatorAttributeName = null;
            String lineKey = null;

            for (int i = 0; i < tmpXDataList.length; i++) {
                lineKey = String.valueOf(i);
                xactuatorAttributeName = tmpXDataList[i];
                yactuatorAttributeName = tmpYActuatorsDataList[i];
                // Input X
                constraints = new GridBagConstraints();
                constraints.gridx = 0;
                constraints.gridy = i;
                constraints.insets = DEFAULT_INSETS;
                constraints.fill = GridBagConstraints.BOTH;
                input = new SimpleActuatorAttributeInputBean(FlowLayout.RIGHT);
                input.setScanServerDeviceName(scanServerDeviceName);
                input.setActuatorAttributeName(xactuatorAttributeName);
                inputMap.put(xactuatorAttributeName, input);
                mainPanel.add(input, constraints);
                input.start();

                // Input Y
                constraints = new GridBagConstraints();
                constraints.gridx = 1;
                constraints.gridy = i;
                constraints.insets = DEFAULT_INSETS;
                constraints.fill = GridBagConstraints.BOTH;
                input = new SimpleActuatorAttributeInputBean(FlowLayout.RIGHT);
                input.setScanServerDeviceName(scanServerDeviceName);
                input.setActuatorAttributeName(yactuatorAttributeName);
                inputMap.put(yactuatorAttributeName, input);
                mainPanel.add(input, constraints);
                input.start();

                // Send dual Button
                constraints = new GridBagConstraints();
                constraints.gridx = 2;
                constraints.gridy = i;
                constraints.insets = DEFAULT_INSETS;
                dualButton = new JButton(SEND_LINE);
                dualButton.setActionCommand(lineKey.toString());
                dualButton.addActionListener(this);
                mainPanel.add(dualButton, constraints);
                buttonList.add(dualButton);

                dualMap.put(lineKey, new String[] { xactuatorAttributeName, yactuatorAttributeName });
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();
        String lineKey = button.getActionCommand();

        if (dualMap.containsKey(lineKey)) {
            String[] dual = dualMap.get(lineKey);
            Collection<SimpleActuatorAttributeInputBean> inputList = new ArrayList<>();
            for (String key : dual) {
                SimpleActuatorAttributeInputBean input = inputMap.get(key);
                if (input != null) {
                    inputList.add(input);
                }
            }

            int result = JOptionPane.OK_OPTION;
            if (confirmation) {
                String messageConfirmation = buildConfirmationMessage(inputList);
                if ((messageConfirmation != null) && !messageConfirmation.isEmpty()) {
                    result = JOptionPane.showConfirmDialog(this, messageConfirmation, "Confirmation",
                            JOptionPane.YES_NO_CANCEL_OPTION);
                } else {
                    JOptionPane.showMessageDialog(this, "No data to send", "Information",
                            JOptionPane.INFORMATION_MESSAGE);
                    result = JOptionPane.CANCEL_OPTION;
                }
            }

            if ((inputList != null) && (result == JOptionPane.OK_OPTION)) {
                for (SimpleActuatorAttributeInputBean input : inputList) {
                    input.send();
                    input.setConfirmation(confirmation);
                }
            }
        }
    }

    private String buildConfirmationMessage(Collection<SimpleActuatorAttributeInputBean> inputList) {
        String message = ObjectUtils.EMPTY_STRING;
        if (inputList != null) {
            for (SimpleActuatorAttributeInputBean input : inputList) {
                input.setConfirmation(false);
                String singleMessage1 = input.getConfirmationMessage();
                if ((singleMessage1 != null) && !singleMessage1.isEmpty()) {
                    message = message + singleMessage1 + ObjectUtils.NEW_LINE;
                }
            }
        }
        return message;
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        try {
            runMain(CurrentScan2DActuatorInput.class, "test/scan/julien");
        } catch (InstantiationException | IllegalAccessException e) {
            // should never happen
            e.printStackTrace();
        }
    }

}
