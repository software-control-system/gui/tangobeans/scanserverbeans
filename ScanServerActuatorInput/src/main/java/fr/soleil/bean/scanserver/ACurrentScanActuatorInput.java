package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;

import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;

public abstract class ACurrentScanActuatorInput extends JScrollPane implements IScanServerListener {

    private static final long serialVersionUID = 414693657088985938L;

    public static final boolean DEFAULT_SEND_SENSOR = false;

    protected static final Insets DEFAULT_INSETS = new Insets(5, 5, 5, 5);
    protected static final String SEND_ALL = "Send all";

    protected final Map<String, SimpleActuatorAttributeInputBean> inputMap;

    // GUI ELEMENTS
    protected final JPanel containPanel;
    protected final JPanel mainPanel;
    protected final JButton sendAll;

    /**
     * The scan Server Device Name
     */
    protected String scanServerDeviceName;

    protected boolean confirmation;

    protected String oldDate;
    protected boolean isSendSensor;

    protected ACurrentScanActuatorInput(boolean isSendSensor, boolean initGUI) {
        super();

        inputMap = new HashMap<>();
        confirmation = true;
        this.isSendSensor = isSendSensor;

        containPanel = new JPanel();
        mainPanel = new JPanel();
        sendAll = new JButton(SEND_ALL);
        if (initGUI) {
            initGUI();
        }
    }

    public void initGUI() {
        containPanel.setLayout(new BorderLayout());
        mainPanel.setLayout(new GridBagLayout());
        containPanel.add(mainPanel, BorderLayout.CENTER);
        containPanel.add(sendAll, BorderLayout.SOUTH);
        sendAll.addActionListener((e) -> {
            sendAll();
        });
        setViewportView(containPanel);
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
        for (SimpleActuatorAttributeInputBean input : inputMap.values()) {
            if (input != null) {
                input.setConfirmation(confirmation);
            }
        }
    }

    public void sendAll() {
        int result = JOptionPane.OK_OPTION;
        if (confirmation) {
            String messageConfirmation = buildConfirmationMessage();
            if ((messageConfirmation != null) && !messageConfirmation.isEmpty()) {
                result = JOptionPane.showConfirmDialog(this, messageConfirmation, "Confirmation",
                        JOptionPane.YES_NO_CANCEL_OPTION);
            } else {
                JOptionPane.showMessageDialog(this, "No data to send", "Information", JOptionPane.INFORMATION_MESSAGE);
                result = JOptionPane.CANCEL_OPTION;
            }
        }

        if (result == JOptionPane.OK_OPTION) {
            Collection<SimpleActuatorAttributeInputBean> collection = inputMap.values();
            for (SimpleActuatorAttributeInputBean input : collection) {
                if (input != null) {
                    input.send();
                    input.setConfirmation(confirmation);
                }
            }
        }
    }

    protected String buildConfirmationMessage() {
        String message = ObjectUtils.EMPTY_STRING;
        Collection<SimpleActuatorAttributeInputBean> collection = inputMap.values();
        String singleMessage = null;
        for (SimpleActuatorAttributeInputBean input : collection) {
            if (input != null) {
                input.setConfirmation(false);
                singleMessage = input.getConfirmationMessage();
                if ((singleMessage != null) && !singleMessage.isEmpty()) {
                    message = message + singleMessage + ObjectUtils.NEW_LINE;
                }
            }
        }
        return message;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public String getScanServerDeviceName() {
        return scanServerDeviceName;
    }

    public void setScanServerDeviceName(String name) {
        if ((scanServerDeviceName != null) && !scanServerDeviceName.equalsIgnoreCase(name)) {
            CurrentScanDataModel.removeScanServerListener(scanServerDeviceName, this);
            clearGUI();
        }
        if ((name != null) && !name.equalsIgnoreCase(scanServerDeviceName)) {
            scanServerDeviceName = name;
            CurrentScanDataModel.addScanServerListener(scanServerDeviceName, this);
        }
    }

    protected void clearGUI() {
        for (SimpleActuatorAttributeInputBean input : inputMap.values()) {
            if (input != null) {
                input.clearGUI();
                input.setModel(null);
                input.setScanServerDeviceName(null);
            }
        }
        mainPanel.removeAll();
        inputMap.clear();
    }

    protected abstract void refreshGUI(String[] tmpXList, String[] tmpXDataList, String[] tmpXActuatorsDataList,
            String[] tmpXActuatorsList, String[] tmpYActuatorsDataList, String[] tmpYActuatorsList);

    protected void refreshGUI() {
        if (CurrentScanDataModel.dateChanged(scanServerDeviceName, oldDate)) {
            oldDate = CurrentScanDataModel.getStartDate(scanServerDeviceName);
            clearGUI();

            String[] tmpXActuatorsDataList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.ACTUATORS_DATA_LIST);

            String[] tmpXActuatorsList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.ACTUATORS_LIST);

            String[] tmpYActuatorsDataList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.YACTUATORS_DATA_LIST);
            String[] tmpYActuatorsList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.YACTUATORS_LIST);

            String[] tmpXSensorsDataList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.SENSORS_DATA_LIST);
            String[] tmpXSensorsList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.SENSORS_LIST);

            String[] tmpXDataList;
            if (isSendSensor) {
                tmpXDataList = new String[tmpXActuatorsDataList.length + tmpXSensorsDataList.length];
            } else {
                tmpXDataList = new String[tmpXActuatorsDataList.length];
            }
            int index = 0;
            for (String data : tmpXActuatorsDataList) {
                tmpXDataList[index++] = data;
            }
            if (isSendSensor) {
                for (String data : tmpXSensorsDataList) {
                    tmpXDataList[index++] = data;
                }
            }

            String[] tmpXList;
            if (isSendSensor) {
                tmpXList = new String[tmpXActuatorsList.length + tmpXSensorsList.length];
            } else {
                tmpXList = new String[tmpXActuatorsList.length];
            }
            index = 0;
            for (String data : tmpXActuatorsList) {
                tmpXList[index++] = data;
            }
            if (isSendSensor) {
                for (String data : tmpXSensorsList) {
                    tmpXList[index++] = data;
                }
            }

            refreshGUI(tmpXList, tmpXDataList, tmpXActuatorsDataList, tmpXActuatorsList, tmpYActuatorsDataList,
                    tmpYActuatorsList);
        }
    }

    @Override
    public void stateChanged(String state) {
        refreshGUI();
    }

    /**
     * Launches the application based on a {@link ACurrentScanActuatorInput} class.
     * 
     * @param beanClass The {@link ACurrentScanActuatorInput} class.
     * @param scanServerDeviceName The scanserver device name to connect to.
     * @throws IllegalAccessException if <code>beanClass</code> or its nullaryconstructor is not accessible.
     * @throws InstantiationException if <code>beanClass</code> represents an abstract class,an interface, an array
     *             class, a primitive type, or void;or if <code>beanClass</code> has no nullary constructor;or if the
     *             instantiation fails for some other reason.
     */
    public static void runMain(Class<? extends ACurrentScanActuatorInput> beanClass, String scanServerDeviceName)
            throws InstantiationException, IllegalAccessException {
        if (beanClass != null) {
            ACurrentScanActuatorInput bean = beanClass.newInstance();
            bean.setScanServerDeviceName(scanServerDeviceName);
            JFrame frame = new JFrame(beanClass.getSimpleName() + ": " + scanServerDeviceName);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setContentPane(bean);
            frame.pack();
            frame.setVisible(true);
        }
    }

}
