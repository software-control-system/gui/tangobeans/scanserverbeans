package fr.soleil.bean.scanserver;

import java.awt.FlowLayout;
import java.awt.Font;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import org.tango.utils.TangoUtil;

import fr.esrf.Tango.DevFailed;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;

public class SimpleActuatorAttributeInputBean extends AbstractTangoBox implements IScanServerListener {

    private static final long serialVersionUID = -4065741384204513016L;

    private static final Font LITTLE_FONT = new Font(Font.SANS_SERIF, Font.PLAIN, 10);

    // GUI ELEMENTS
    private final JLabel attributeLabel;
    private final Label attributeRead;

    private final TextFieldButton attributeField;
    private String attributeName;
    private String actuatorAttributeName;
    private int actuatorIndex;
    private int actuatorXIndex;
    private int actuatorYIndex;
    private String scanServerDeviceName;

    public SimpleActuatorAttributeInputBean(int alignement) {
        super();
        attributeLabel = new JLabel();
        attributeRead = new Label();
        attributeField = new TextFieldButton();
        actuatorAttributeName = "actuator_1_1";
        actuatorIndex = actuatorXIndex = actuatorYIndex = -1;
        setLayout(new FlowLayout(alignement));
        initGUI();
    }

    @Override
    public void start() {
        if ((model != null) && !model.isEmpty()) {
            super.start();
        }
    }

    public String getConfirmationMessage() {
        String message = null;
        String label = attributeLabel.getText();
        String value = attributeField.getText();
        if ((value != null) && !value.trim().isEmpty()) {
            message = "send " + value + " on " + label + " ?";
        }
        return message;
    }

    public void setConfirmation(boolean confirmation) {
        stringBox.setConfirmation(attributeField, confirmation);
    }

    public boolean isConfirmation() {
        return stringBox.isConfirmation(attributeField);
    }

    public int getActuatorIndex() {
        return actuatorIndex;
    }

    public int getActuatorXIndex() {
        return actuatorXIndex;
    }

    public void setActuatorXIndex(int x) {
        this.actuatorXIndex = x;
    }

    public int getActuatorYIndex() {
        return actuatorYIndex;
    }

    public void setActuatorYIndex(int y) {
        this.actuatorYIndex = y;
    }

    public void setActuatorIndex(int index) {
        this.actuatorIndex = index;
        double value = CurrentScanDataModel.getValueAtIndex(scanServerDeviceName, actuatorAttributeName, index);
        if (!Double.isNaN(value)) {
            attributeField.setText(String.valueOf(value));
        }
    }

    public void setActuatorIndex(int x, int y) {
        setActuatorXIndex(x);
        setActuatorYIndex(y);
        double value = CurrentScanDataModel.getValueAtIndex(scanServerDeviceName, actuatorAttributeName, x, y);
        if (!Double.isNaN(value)) {
            attributeField.setText(String.valueOf(value));
        }
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String name) {
        if (name == null) {
            return;
        }

        if ((this.attributeName == null) || !this.attributeName.equals(name)) {
            this.attributeName = name;
        }
    }

    public void computeCompleteAttributeName() {
        // Fixe bug 0026823 read the label and not the attribute of scanserver
        if ((scanServerDeviceName != null) && (actuatorAttributeName != null)) {
            String name = CurrentScanDataModel.getAttributeLabel(scanServerDeviceName, actuatorAttributeName);
            // It can be an alias
            if ((name != null) && !name.isEmpty()) {
                try {
                    name = name.replace("(sensor)", "");
                    String fullattributeName = TangoUtil.getfullAttributeNameForAttribute(name);
                    if ((fullattributeName != null) && !fullattributeName.isEmpty()) {
                        setAttributeName(TangoDeviceHelper.getEntityName(fullattributeName));
                        String deviceName = TangoDeviceHelper.getDeviceName(fullattributeName);
                        setModel(deviceName);
                    }
                } catch (DevFailed e) {
                }
            }
        }

    }

    public String getScanServerDeviceName() {
        return scanServerDeviceName;
    }

    public void setScanServerDeviceName(String name) {
        if ((scanServerDeviceName != null) && !scanServerDeviceName.equalsIgnoreCase(name)) {
            CurrentScanDataModel.removeScanServerListener(scanServerDeviceName, this);
        }
        scanServerDeviceName = name;
        computeCompleteAttributeName();
    }

    public String getActuatorAttributeName() {
        return actuatorAttributeName;
    }

    public void setActuatorAttributeName(String name) {
        this.actuatorAttributeName = name;
        computeCompleteAttributeName();
    }

    protected boolean isValidConfiguration() {
        return (getModel() != null) && (!getModel().isEmpty()) && (attributeName != null) || (!attributeName.isEmpty());
    }

    protected boolean isActuatorValidConfiguration() {
        return (model != null) && (!model.isEmpty()) && (actuatorAttributeName != null)
                && (!actuatorAttributeName.isEmpty());
    }

    @Override
    protected void clearGUI() {
        super.cleanWidget(attributeRead);
        super.cleanWidget(attributeField);
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

    /**
     * Initializes the GUI.
     */
    public void initGUI() {
        // Add Label
        add(attributeLabel);
        attributeLabel.setText("Not connected");
        attributeLabel.setFont(LITTLE_FONT);

        // Read field
        add(attributeRead);
        attributeRead.setPreferredSize(100, 25);

        // Add NumberField
        add(attributeField);
        attributeField.setPreferredSize(120, 25);
        attributeField.setAlwaysUpdateTextField(true);

        // setUserEnabled(true);
        setConfirmation(true);
        stringBox.setColorEnabled(attributeField, false);
        stringBox.setSettable(attributeField, false);

    }

    public void send() {
        String value = attributeField.getText();
        if ((value != null) && !value.isEmpty()) {
            attributeField.send();
        }
    }

    @Override
    protected void refreshGUI() {
        if (isValidConfiguration()) {
            attributeLabel.setText(getModel() + "/" + attributeName);
            attributeLabel.setToolTipText(getModel() + "/" + actuatorAttributeName);
            TangoKey tangoKey = generateAttributeKey(attributeName);
            setWidgetModel(attributeRead, stringBox, tangoKey);

            tangoKey = generateWriteAttributeKey(attributeName);
            setWidgetModel(attributeField, stringBox, tangoKey);
        }
    }

    @Override
    public void stateChanged(String state) {
        boolean enable = !CurrentScanDataModel.isMoving(state);
        attributeField.setEditable(enable);
        attributeField.setEnabled(enable);
        stringBox.setColorEnabled(attributeField, false);
        stringBox.setSettable(attributeField, enable);
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        SimpleActuatorAttributeInputBean bean = new SimpleActuatorAttributeInputBean(FlowLayout.CENTER);
        bean.setActuatorAttributeName("actuator_1_1");
        bean.setScanServerDeviceName("ica/salsa/scan.1");
        bean.start();

        bean.setActuatorIndex(5);
        bean.setActuatorIndex(10, 5);

        JFrame frame = new JFrame();
        frame.setContentPane(bean);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
