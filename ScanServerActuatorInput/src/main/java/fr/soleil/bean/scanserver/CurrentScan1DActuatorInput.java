package fr.soleil.bean.scanserver;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;

public class CurrentScan1DActuatorInput extends ACurrentScanActuatorInput {

    private static final long serialVersionUID = 653879530971520488L;

    public CurrentScan1DActuatorInput(boolean isSendSensor) {
        super(isSendSensor, true);
    }

    public CurrentScan1DActuatorInput() {
        this(DEFAULT_SEND_SENSOR);
    }

    public void setIndex(int xindex) {
        for (SimpleActuatorAttributeInputBean input : inputMap.values()) {
            if (input != null) {
                input.setActuatorIndex(xindex);
            }
        }
    }

    @Override
    protected void refreshGUI(String[] tmpXList, String[] tmpXDataList, String[] tmpXActuatorsDataList,
            String[] tmpXActuatorsList, String[] tmpYActuatorsDataList, String[] tmpYActuatorsList) {
        String[] tmpActuatorsDataList = null;
        String[] tmpActuatorsList = null;
        if ((tmpYActuatorsDataList != null) && (tmpYActuatorsDataList.length > 0)) {
            tmpActuatorsDataList = tmpYActuatorsDataList;
            tmpActuatorsList = tmpYActuatorsList;
        } else {
            tmpActuatorsDataList = tmpXDataList;
            tmpActuatorsList = tmpXList;
        }

        if ((tmpActuatorsDataList != null) && (tmpActuatorsDataList.length > 0) && (tmpActuatorsList != null)
                && (tmpActuatorsList.length > 0) && (tmpActuatorsDataList.length == tmpActuatorsList.length)) {
            GridBagConstraints constraints = null;
            SimpleActuatorAttributeInputBean input = null;

            String actuatorAttributeName = null;
            for (int i = 0; i < tmpActuatorsDataList.length; i++) {
                actuatorAttributeName = tmpActuatorsDataList[i];
                constraints = new GridBagConstraints();
                constraints.gridx = 0;
                constraints.gridy = i;
                constraints.insets = DEFAULT_INSETS;
                constraints.fill = GridBagConstraints.BOTH;
                input = new SimpleActuatorAttributeInputBean(FlowLayout.RIGHT);
                input.setScanServerDeviceName(scanServerDeviceName);
                input.setActuatorAttributeName(actuatorAttributeName);
                inputMap.put(actuatorAttributeName, input);
                mainPanel.add(input, constraints);
                input.start();
            }
        }
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        try {
            runMain(CurrentScan1DActuatorInput.class, "ica/salsa/scan.1");
        } catch (InstantiationException | IllegalAccessException e) {
            // should never happen
            e.printStackTrace();
        }
    }

}
