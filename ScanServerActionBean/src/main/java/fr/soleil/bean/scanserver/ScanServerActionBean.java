package fr.soleil.bean.scanserver;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.NumberComboBox;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.model.scanserver.Action;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;

/**
 * @author Alike
 * 
 *         Class relative to the status bar view.
 */
public class ScanServerActionBean extends AbstractTangoBox implements IScanServerListener {

    private static final long serialVersionUID = -4254614759920751131L;

    private String oldDate = null;

    // GUI Elements
    // Define the action type
    private final NumberComboBox comboActionType = new NumberComboBox();
    // Select a sensor
    private final NumberComboBox comboSensor = new NumberComboBox();
    // Select a actuator
    private final NumberComboBox comboActuator = new NumberComboBox();
    // Button Goto
    // Bug Error display in the button
    private final JButton buttonGoTo = new JButton();

    private boolean confirmation = false;

    private NumberScalarBox numberScalarBox = new NumberScalarBox();

    // private final StringButton buttonGoTo = new StringButton();

    /**
     * Constructor
     */
    public ScanServerActionBean() {
        setConfirmation(true);
        initGUI();
    }

    public String getScanServerName() {
        return getModel();
    }

    public void setScanServerName(String ascanServerName) {
        this.setModel(ascanServerName);
    }

    /**
     * Initializes the GUI.
     */
    public void initGUI() {

        this.setLayout(new GridBagLayout());
        comboActionType.setEditable(false);
        buttonGoTo.setText("Go to");
        buttonGoTo.setToolTipText("Not connected");
        // buttonGoTo.setButtonLook(true);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.BOTH;
        add(new JLabel("Action type "), constraints);

        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.BOTH;
        add(comboActionType, constraints);
        initCombo(comboActionType, Action.labelArray, Action.valueArray);

        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.BOTH;
        add(new JLabel("Sensor "), constraints);

        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.BOTH;
        add((JComponent) comboSensor, constraints);
        comboSensor.setEditable(false);

        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.fill = GridBagConstraints.BOTH;
        add(new JLabel("Actuator "), constraints);

        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.fill = GridBagConstraints.BOTH;
        add((JComponent) comboActuator, constraints);
        comboActuator.setEditable(false);

        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;
        add((JComponent) buttonGoTo, constraints);
        buttonGoTo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                executeAction();
            }
        });
    }

    private void initCombo(ComboBox combo, String[] labelList, String[] valueList) {
        // System.out.println("initCombo=" + Arrays.toString(labelList) + " value="
        // + Arrays.toString(valueList));
        if ((combo != null) && (labelList != null) && (valueList != null)
                && (valueList.length == labelList.length)) {
            combo.removeAllItems();
            combo.setDisplayedList(labelList);
            combo.setObjectArray(valueList);
            combo.repaint();
        }
    }

    public void setUserEnabled(boolean enabled) {
        comboActionType.setEnabled(enabled);
        buttonGoTo.setEnabled(enabled);

        if (!enabled) {
            comboSensor.setEnabled(enabled);
            comboActuator.setEnabled(enabled);
        }
        else {
            int index = comboActionType.getSelectedIndex();
            if (index == 0) {
                buttonGoTo.setEnabled(false);
            }

            Action action = Action.getAction(index);
            if (action != null) {
                comboSensor.setEnabled(action.isSensorRequired());
                comboActuator.setEnabled(action.isActuatorRequired());
            }
        }
    }

    public void setLabel() {
        int index = comboActionType.getSelectedIndex();
        Action action = Action.getAction(index);
        // System.out.println("action=" + action);
        if (action != null) {
            buttonGoTo.setToolTipText(action.getActionDescription());
            buttonGoTo.setText("Go to");
        }

    }

    @Override
    protected void clearGUI() {
        // cleanWidget(comboActionType);
        // cleanWidget(comboSensor);
        // cleanWidget(comboActuator);
        // cleanWidget(buttonGoTo);
        // buttonGoTo.setToolTipText("Not connected");
        CurrentScanDataModel.removeScanServerListener(model, this);
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            TangoKey tangoKey = null;

            tangoKey = generateWriteAttributeKey(CurrentScanDataModel.TYPE_ACTION);
            setWidgetModel(comboActionType, numberScalarBox, tangoKey);

            tangoKey = generateWriteAttributeKey(CurrentScanDataModel.SENSOR_ACTION);
            setWidgetModel(comboSensor, numberScalarBox, tangoKey);

            tangoKey = generateWriteAttributeKey(CurrentScanDataModel.ACTUATOR_ACTION);
            setWidgetModel(comboActuator, numberScalarBox, tangoKey);

            // tangoKey = generateCommandKey(CurrentScanDataModel.EXECUTE_ACTION);
            // setWidgetModel(buttonGoTo, stringBox, tangoKey);
            // buttonGoTo.setText("Go to");

            CurrentScanDataModel.addScanServerListener(model, this);
        }
    }

    private void fillCombo(NumberComboBox combo, String attributeName) {
        String[] optionList = null;
        String[] valueList = null;

        optionList = CurrentScanDataModel.readAttributeList(getModel(), attributeName);
        if (optionList != null && optionList.length > 0) {
            valueList = new String[optionList.length];
            for (int i = 0; i < optionList.length; i++) {
                valueList[i] = String.valueOf(i);
            }
            initCombo(combo, optionList, valueList);
            // combo.setValueList(valueList);
            // combo.setDisplayedList(optionList);

        }
    }

    @Override
    public void stateChanged(String state) {
        // System.out.println("stateChanged= " + state);
        String scanServerName = getModel();
        if (CurrentScanDataModel.dateChanged(scanServerName, oldDate)) {
            oldDate = CurrentScanDataModel.getStartDate(scanServerName);
            fillCombo(comboSensor, CurrentScanDataModel.SENSORS_LIST);
            fillCombo(comboActuator, CurrentScanDataModel.ACTUATORS_LIST);
        }
        setLabel();
        setUserEnabled(!CurrentScanDataModel.isMoving(state));
    }

    private void executeAction() {
        String scanServerName = getModel();
        int result = JOptionPane.showConfirmDialog(this, buttonGoTo.getToolTipText(),
                scanServerName + "/" + CurrentScanDataModel.EXECUTE_ACTION,
                JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.OK_OPTION) {
            if (TangoDeviceHelper.isDeviceRunning(scanServerName)) {
                DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
                if (proxy != null) {
                    try {
                        proxy.command_inout(CurrentScanDataModel.EXECUTE_ACTION);
                    }
                    catch (DevFailed e) {
                        String message = DevFailedUtils.toString(e);
                        JOptionPane.showMessageDialog(this, message);
                    }
                    catch (Exception e) {
                        String message = e.getMessage();
                        JOptionPane.showMessageDialog(this, message);
                    }
                }
            }
        }
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

    public void setConfirmation(boolean confirmation) {
        // stringBox.setConfirmation(buttonGoTo, confirmation);
        this.confirmation = confirmation;
    }

    public boolean isConfirmation() {
        // return stringBox.isConfirmation(buttonGoTo);
        return confirmation;
    }

    public static void main(String[] args) {

        ScanServerActionBean bean = new ScanServerActionBean();
        bean.setScanServerName("test/scan/julien");
        bean.start();

        JFrame frame = new JFrame();
        frame.setContentPane(bean);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
