package fr.soleil.bean.scanserver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.soleil.comete.swing.Label;

public class SimpleDateFormatLabel extends Label {

    /**
     * 
     */
    private static final long serialVersionUID = -6871815172484404046L;

    @Override
    public void setText(String text) {
        SimpleDateFormat fromUser = new SimpleDateFormat("yyy-MMM-dd HH:mm:ss");
        SimpleDateFormat format = new SimpleDateFormat("H:mm:s");
        try {
            Date userDate = fromUser.parse(text);
            super.setText(format.format(userDate));
        } catch (ParseException e) {
            // IGNORE PARSE ERRORS
        }
    }
}
