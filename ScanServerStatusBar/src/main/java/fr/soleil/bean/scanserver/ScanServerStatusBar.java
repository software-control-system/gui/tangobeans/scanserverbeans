package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.widget.IAutoScrolledComponent.ScrollMode;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.ProgressBar;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.swing.util.ErrorNotificationDelegate;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.scalar.INumberTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.file.BatchExecutor;
import fr.soleil.model.scanserver.CurrentScanDataModel;

/**
 * @author Alike
 * 
 *         Class relative to the status bar view.
 */
public class ScanServerStatusBar extends AbstractTangoBox {

    private static final long serialVersionUID = -6600067056280163015L;

    /**
     * small Font
     */
    private static final CometeFont SMALL_FONT = new CometeFont(Font.DIALOG, Font.PLAIN, 10);

    private final JButton scanServerLabel = new JButton();

    /**
     * Map of Label
     */
    private final Map<String, Label> labelMap = new HashMap<String, Label>();

    /**
     * The progress bar DEAD_TIME_PERCENT.
     */
    private final ProgressBar scanDeadTime = new ProgressBar();

    /**
     * The progress bar SCAN_COMPLETION.
     */
    private final ProgressBar scanCompletion = new ProgressBar();
    private final Label scanNumberLabel = new Label();
    private final Label pointNumberLabel = new Label();
    private final Label pointNumberLabel2 = new Label();
    private final Label yPointLabel = new Label();

    private final NumberScalarBox numberBox = new NumberScalarBox();

    private static final BatchExecutor batchExecutor = new BatchExecutor();

    private String executedBatchFile = null;

    private final AutoScrolledTextField statusField = new AutoScrolledTextField();

    /**
     * Constructor
     */
    public ScanServerStatusBar() {
        initializeLayout();
    }

    public String getExecutedBatchFile() {
        return executedBatchFile;
    }

    public void setExecutedBatchFile(String executedBatchFile) {
        this.executedBatchFile = executedBatchFile;
        if (executedBatchFile != null) {
            if (executedBatchFile.endsWith("{DEVICE}")) {
                executedBatchFile = executedBatchFile.substring(0, executedBatchFile.lastIndexOf("{DEVICE}"));
            }
            if (!executedBatchFile.isEmpty()) {
                batchExecutor.setBatch(executedBatchFile);
            }
        }
    }

    /**
     * When control Button panel is actionned
     */
    public void controlPanelActionPerformed() {
        // System.out.println("batch=" + batchExecutor.getBatch());
        if (model != null) {
            List<String> parameter = batchExecutor.getBatchParameters();
            if (parameter == null) {
                parameter = new ArrayList<String>();
            }
            parameter.clear();
            parameter.add(model);
            batchExecutor.setBatchParameters(parameter);
            batchExecutor.execute();
        }
    }

    /**
     * Apply the "small" style for a label.
     * 
     * @param c
     */
    public void applySmallStyle(Label c) {
        // c.setCometeFont(smallFont);
        c.setHorizontalAlignment(JLabel.CENTER);
    }

    public String getScanServerName() {
        return model;
    }

    public void setScanServerName(String ascanServerName) {
        setModel(ascanServerName);
    }

    /**
     * Initialize the layout.
     */
    private void initializeLayout() {
        setLayout(new GridBagLayout());
        Insets defaultInset = new Insets(5, 10, 5, 10);

        // Scan server name Cell 00
        GridBagConstraints cell00 = new GridBagConstraints();
        cell00.gridx = 0;
        cell00.gridy = 0;
        cell00.gridwidth = 2;
        cell00.insets = defaultInset;
        add(scanServerLabel, cell00);

        scanServerLabel.setToolTipText("Open control panel");
        scanServerLabel.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controlPanelActionPerformed();
            }
        });

        // Scan state Cell 10
        GridBagConstraints cell10 = new GridBagConstraints();
        cell10.gridx = 2;
        cell10.gridy = 0;
        cell10.insets = defaultInset;
        cell10.fill = GridBagConstraints.BOTH;
        add(getStateLabel(), cell10);
        labelMap.put(CurrentScanDataModel.SCAN_STATE, getStateLabel());

        // Scan status Cell 01
        GridBagConstraints cell01 = new GridBagConstraints();
        cell01.gridx = 0;
        cell01.gridy = 1;
        cell01.gridwidth = 3;
        cell01.gridheight = 1;
        cell01.insets = defaultInset;
        statusField.setMinimumSize(new Dimension(200, 15));
        statusField.setColumns(20);
        statusField.setScrollStepsTime(20);
        statusField.setCometeFont(SMALL_FONT);
        statusField.setEditable(false);
        statusField.setHorizontalAlignment(JLabel.LEFT);
        statusField.setScrollMode(ScrollMode.PENDULAR);
        add(statusField, cell01);

        // Run Start Date Label Cell 20
        GridBagConstraints cell20 = new GridBagConstraints();
        cell20.gridx = 3;
        cell20.gridy = 0;
        cell20.insets = defaultInset;
        // Run start date
        JLabel lblrunStartText = new JLabel("Run start date", JLabel.CENTER);
        add(lblrunStartText, cell20);

        // Run start date Cell 21
        GridBagConstraints cell21 = new GridBagConstraints();
        cell21.gridx = 3;
        cell21.gridy = 1;
        cell21.insets = defaultInset;
        addFormattedLabel(CurrentScanDataModel.RUN_START_DATE, cell21);

        // Scan Start Date Label Cell 30
        GridBagConstraints cell30 = new GridBagConstraints();
        cell30.gridx = 4;
        cell30.gridy = 0;
        cell30.insets = defaultInset;
        JLabel lblscanEndText = new JLabel("Scan start date", JLabel.CENTER);
        add(lblscanEndText, cell30);

        // Scan start date Cell 31
        GridBagConstraints cell31 = new GridBagConstraints();
        cell31.gridx = 4;
        cell31.gridy = 1;
        cell31.insets = defaultInset;
        addFormattedLabel(CurrentScanDataModel.SCAN_START_DATE, cell31);

        // Run Remaining time Label Cell 40
        GridBagConstraints cell40 = new GridBagConstraints();
        cell40.gridx = 5;
        cell40.gridy = 0;
        cell40.insets = defaultInset;
        JLabel lblrunRemainginText = new JLabel("Run remaining time", JLabel.CENTER);
        add(lblrunRemainginText, cell40);

        // Run Remaining time Cell 41
        GridBagConstraints cell41 = new GridBagConstraints();
        cell41.gridx = 5;
        cell41.gridy = 1;
        cell41.insets = defaultInset;
        addLabel(CurrentScanDataModel.RUN_REMAINING_TIME, cell41);

        // Scan Remaining time Label Cell 50
        GridBagConstraints cell50 = new GridBagConstraints();
        cell50.gridx = 6;
        cell50.gridy = 0;
        cell50.insets = defaultInset;
        JLabel lblscanRemainginText = new JLabel("Scan remaining time", JLabel.CENTER);
        add(lblscanRemainginText, cell50);

        // Scan Remaining time Cell 51
        GridBagConstraints cell51 = new GridBagConstraints();
        cell51.gridx = 6;
        cell51.gridy = 1;
        cell51.insets = defaultInset;
        addLabel(CurrentScanDataModel.SCAN_REMAINING_TIME, cell51);

        // DeadTimePercent time Cell 60
        GridBagConstraints cell60 = new GridBagConstraints();
        cell60.gridx = 7;
        cell60.gridy = 0;
        cell60.insets = defaultInset;
        JLabel deadTimeLabel = new JLabel("Dead time", JLabel.CENTER);
        add(deadTimeLabel, cell60);

        // ScanDeadTime progress bar Cell 61
        GridBagConstraints cell61 = new GridBagConstraints();
        cell61.gridx = 7;
        cell61.gridy = 1;
        cell61.insets = defaultInset;
        scanDeadTime.setValue(0);
        scanDeadTime.setMinimumSize(new Dimension(100, 15));
        add(scanDeadTime, cell61);

        // DeadTimePercent time Cell 70
        GridBagConstraints cell70 = new GridBagConstraints();
        cell70.gridx = 8;
        cell70.gridy = 0;
        cell70.insets = defaultInset;
        JLabel lblProgress = new JLabel("Scan completion", JLabel.CENTER);
        add(lblProgress, cell70);

        // The progress bar Cell 71
        GridBagConstraints cell71 = new GridBagConstraints();
        cell71.gridx = 8;
        cell71.gridy = 1;
        cell71.insets = defaultInset;
        scanDeadTime.setValue(0);
        scanCompletion.setValue(0);
        scanCompletion.setMinimumSize(new Dimension(100, 15));
        add(scanCompletion, cell71);

        // Scan Number Label Cell 80
        GridBagConstraints cell80 = new GridBagConstraints();
        cell80.gridx = 9;
        cell80.gridy = 0;
        cell80.insets = defaultInset;
        add(new JLabel(" Scan number"), cell80);

        // Scan Number Cell 81
        GridBagConstraints cell81 = new GridBagConstraints();
        cell81.gridx = 9;
        cell81.gridy = 1;
        cell81.insets = defaultInset;
        scanNumberLabel.setText("-/-");
        add(scanNumberLabel, cell81);

        // Point Number Cell 82
        GridBagConstraints cell82 = new GridBagConstraints();
        cell82.gridx = 10;
        cell82.gridy = 0;
        cell82.insets = defaultInset;
        add(new JLabel("X Point"), cell82);

        // Point Number Cell 83
        GridBagConstraints cell83 = new GridBagConstraints();
        cell83.gridx = 10;
        cell83.gridy = 1;
        cell83.insets = defaultInset;
        pointNumberLabel.setText("-/-");
        add(pointNumberLabel, cell83);

        // Point Number Cell 84
        GridBagConstraints cell84 = new GridBagConstraints();
        cell84.gridx = 11;
        cell84.gridy = 0;
        cell84.insets = defaultInset;
        yPointLabel.setText("Y Point ");
        add(yPointLabel, cell84);

        // Point Number Cell 85
        GridBagConstraints cell85 = new GridBagConstraints();
        cell85.gridx = 11;
        cell85.gridy = 1;
        cell85.insets = defaultInset;
        pointNumberLabel2.setText("-/-");
        add(pointNumberLabel2, cell85);

    }

    private ITextComponent generateVirtualTarget(boolean isFirst, Label label) {
        final Label labelValue = label;
        final boolean isFirstValue = isFirst;
        ITextComponent result = new AbstractVirtualDualTextComponent() {
            private final ErrorNotificationDelegate errorNotificationDelegate = new ErrorNotificationDelegate(
                    labelValue);

            @Override
            public void setText(String text) {
                String[] values = labelValue.getText().split("/");

                if (values.length == 2) {
                    if (isFirstValue) {
                        labelValue.setText(text + "/" + values[1]);
                    } else {
                        labelValue.setText(values[0] + "/" + text);
                    }
                }
            }

            @Override
            public String getText() {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void setCometeBackground(CometeColor color) {
                labelValue.setCometeBackground(color);
            }

            @Override
            public void notifyForError(String message, Throwable error) {
                errorNotificationDelegate.notifyForError(message, error);
            }

            @Override
            public IDrawingThreadManager getDrawingThreadManager() {
                return EDTManager.INSTANCE;
            }

            @Override
            public boolean shouldReceiveDataInDrawingThread() {
                return true;
            }
        };
        return result;
    }

    private INumberTarget getINumberTarget() {

        INumberTarget result = new INumberTarget() {

            @Override
            public void addMediator(Mediator<?> mediator) {
                // TODO Auto-generated method stub

            }

            @Override
            public void removeMediator(Mediator<?> mediator) {
                // TODO Auto-generated method stub

            }

            @Override
            public Number getNumberValue() {
                return 1;
            }

            @Override
            public void setNumberValue(Number value) {
                pointNumberLabel2.setVisible(value.intValue() != 1);
                yPointLabel.setVisible(value.intValue() != 1);

            }
        };
        return result;

    }

    private void addLabel(final String label, GridBagConstraints constraints) {
        Label target = new Label();
        target.setMinimumSize(new Dimension(90, 15));
        target.setToolTipText("SCANSERVER/" + label);
        target.setText("-");
        applySmallStyle(target);
        add(target, constraints);
        labelMap.put(label, target);
    }

    private void addFormattedLabel(final String label, GridBagConstraints constraints) {
        Label target = new SimpleDateFormatLabel();
        target.setMinimumSize(new Dimension(150, 15));
        target.setToolTipText("SCANSERVER/" + label);
        target.setText("-");
        applySmallStyle(target);
        add(target, constraints);
        labelMap.put(label, target);
    }

    @Override
    protected void clearGUI() {

        cleanWidget(statusField);
        cleanStateModel();

        Collection<Label> labelCollection = labelMap.values();
        Iterator<Label> labelIterator = labelCollection.iterator();

        while (labelIterator.hasNext()) {
            Label label = labelIterator.next();
            cleanWidget(label);
        }

        cleanWidget(scanDeadTime);
        cleanWidget(scanCompletion);
    }

    @Override
    protected void refreshGUI() {
        if ((model != null) && !model.isEmpty()) {
            setWidgetModel(statusField, stringBox, generateAttributeKey("Status"));
            setStateModel();

            scanServerLabel.setText(model);
            labelMap.get(CurrentScanDataModel.SCAN_STATE).setToolTipText(model);
            Set<String> keySet = labelMap.keySet();
            Iterator<String> keyIterator = keySet.iterator();
            TangoKey tangoKey = null;
            while (keyIterator.hasNext()) {
                String key = keyIterator.next();
                Label label = labelMap.get(key);
                tangoKey = generateAttributeKey(key);
                setWidgetModel(label, stringBox, tangoKey);
            }

            // Progress bar
            tangoKey = generateAttributeKey(CurrentScanDataModel.DEAD_TIME_PERCENT);
            setWidgetModel(scanDeadTime, numberBox, tangoKey);

            tangoKey = generateAttributeKey(CurrentScanDataModel.SCAN_COMPLETION);
            setWidgetModel(scanCompletion, numberBox, tangoKey);

            TangoKey scanNumberKey = generateAttributeKey("scanNumber");
            TangoKey scanNumberWriteKey = generateWriteAttributeKey("scanNumber");
            setWidgetModel(generateVirtualTarget(true, scanNumberLabel), stringBox, scanNumberKey);
            setWidgetModel(generateVirtualTarget(false, scanNumberLabel), stringBox, scanNumberWriteKey);

            TangoKey pointNumberKey = generateAttributeKey("pointNumber");
            TangoKey pointNumberWriteKey = generateWriteAttributeKey("pointNumber");

            ITextTarget virtualTarget1 = generateVirtualTarget(true, pointNumberLabel);
            ITextTarget virtualTarget2 = generateVirtualTarget(false, pointNumberLabel);

            setWidgetModel(virtualTarget1, stringBox, pointNumberKey);
            setWidgetModel(virtualTarget2, stringBox, pointNumberWriteKey);

            TangoKey pointNumberKey2 = generateAttributeKey("pointNumber2");
            TangoKey pointNumberWriteKey2 = generateWriteAttributeKey("pointNumber2");

            ITextTarget targetVirtual1 = generateVirtualTarget(true, pointNumberLabel2);
            ITextTarget targetVirtual2 = generateVirtualTarget(false, pointNumberLabel2);

            setWidgetModel(targetVirtual1, stringBox, pointNumberKey2);
            setWidgetModel(targetVirtual2, stringBox, pointNumberWriteKey2);

            TangoKey scanTypeKey = generateAttributeKey("scanType");
            setWidgetModel(getINumberTarget(), numberBox, scanTypeKey);

        }
    }

    @Override
    protected void onConnectionError() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) {

        ScanServerStatusBar bean = new ScanServerStatusBar();
        bean.setScanServerName("ica/salsa/scan.1");
        bean.start();

        ScanServerStatusBar bean2 = new ScanServerStatusBar();
        bean2.setScanServerName("ica/salsa/scan.1");
        bean2.start();

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        // mainPanel.add(bean, BorderLayout.NORTH);
        mainPanel.add(bean2, BorderLayout.SOUTH);

        JFrame frame = new JFrame();
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

}
