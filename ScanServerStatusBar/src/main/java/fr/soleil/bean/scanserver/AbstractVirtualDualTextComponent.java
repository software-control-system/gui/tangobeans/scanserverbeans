package fr.soleil.bean.scanserver;

import fr.soleil.comete.definition.data.target.scalar.ITextComponent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.data.mediator.Mediator;

public abstract class AbstractVirtualDualTextComponent implements ITextComponent {

    @Override
    public String getText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addMediator(Mediator<?> mediator) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean hasFocus() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setOpaque(boolean opaque) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isOpaque() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public CometeColor getCometeBackground() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setCometeForeground(CometeColor color) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeColor getCometeForeground() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setEnabled(boolean enabled) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setCometeFont(CometeFont font) {
        // TODO Auto-generated method stub

    }

    @Override
    public CometeFont getCometeFont() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setToolTipText(String text) {
        // TODO Auto-generated method stub

    }

    @Override
    public String getToolTipText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setVisible(boolean visible) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isVisible() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setHorizontalAlignment(int halign) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getHorizontalAlignment() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setSize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setPreferredSize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getWidth() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getHeight() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void setLocation(int x, int y) {
        // TODO Auto-generated method stub

    }

    @Override
    public int getX() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getY() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isEditingData() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setTitledBorder(String title) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addMouseListener(IMouseListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeMouseListener(IMouseListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public void removeAllMouseListeners() {
        // TODO Auto-generated method stub

    }

}
