package fr.soleil.model.scanserver;

public interface ICurrentScanResultListener {

    public void startIndexSelectedChanged(int start);

    public void endIndexSelectedChanged(int end);

    public void startPointSelectedChanged(int startX, int startY);

    public void endPointSelectedChanged(int endX, int endY);

    public boolean isSelectionActivated();
}
