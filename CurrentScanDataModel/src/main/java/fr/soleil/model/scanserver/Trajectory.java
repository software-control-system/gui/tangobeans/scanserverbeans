package fr.soleil.model.scanserver;

public class Trajectory {

    private double from = Double.NaN;
    private double to = Double.NaN;
    private boolean relative = false;
    private double[] completeTrajectory = null;
    private int stepNumber = 0;
    private double integrationTime = Double.NaN;
    private boolean customTrajectory = false;
    private String actuator = null;

    public String getActuator() {
        return actuator;
    }

    public void setActuator(String actuator) {
        this.actuator = actuator;
    }

    public boolean isCustomTrajectory() {
        return customTrajectory;
    }

    public void setCustomTrajectory(boolean customTrajectory) {
        this.customTrajectory = customTrajectory;
    }

    public boolean isRelative() {
        return relative;
    }

    public void setRelative(boolean relative) {
        this.relative = relative;
    }

    public double getIntegrationTime() {
        return integrationTime;
    }

    public void setIntegrationTime(double integrationTime) {
        this.integrationTime = integrationTime;
    }

    public double getFrom() {
        from = Double.NaN;
        if (completeTrajectory != null && completeTrajectory.length > 0) {
            if (relative) {
                from = 0;
            }
            else {
                from = completeTrajectory[0];
            }
        }
        return from;
    }

    public double getTo() {
        to = Double.NaN;
        if (completeTrajectory != null && completeTrajectory.length > 0) {
            double endValue = completeTrajectory[completeTrajectory.length - 1];
            if (relative) {
                double beginValue = completeTrajectory[0];
                to = endValue - beginValue;
            }
            else {
                to = endValue;
            }
        }
        return to;
    }

    public int getStepNumber() {
        stepNumber = 0;
        if (completeTrajectory != null) {
            stepNumber = completeTrajectory.length;
        }
        return stepNumber;
    }

    public void setCompleteTrajectory(double[] completeTrajectory) {
        this.completeTrajectory = completeTrajectory;
    }

    public double[] getCompleteTrajectory() {
        double[] trajectoryValues = completeTrajectory;
        if (relative && completeTrajectory != null && completeTrajectory.length > 0) {
            trajectoryValues = new double[completeTrajectory.length];
            double offset = completeTrajectory[0];
            for (int i = 0; i < completeTrajectory.length; i++) {
                trajectoryValues[i] = completeTrajectory[i] - offset;
            }
        }

        return trajectoryValues;
    }
}
