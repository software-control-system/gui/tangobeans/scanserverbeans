package fr.soleil.model.scanserver;

public class Axis {

    public final static String X = "X";
    public final static String NONE = "";
    public final static String Y1 = "Y1";
    public final static String Y2 = "Y2";
    public final static String Z = "Z";

    private String axis = NONE;
    private boolean spectrum = false;

    public boolean isSpectrum() {
        return spectrum;
    }

    public void setSpectrum(boolean spectrum) {
        this.spectrum = spectrum;
    }

    public String getAxis() {
        return axis;
    }

    public void setAxis(String anAxis) {
        this.axis = anAxis;
    }

    @Override
    public String toString() {
        return axis;
    }

    public static String getDefaultAxis(boolean isSpectrum, String defaultAxis) {

        if (defaultAxis.equals(Axis.NONE)) {
            return defaultAxis;
        }

        if (isSpectrum && defaultAxis.equals(Axis.Z)) {
            return Axis.Y1;
        }

        return defaultAxis;
    }

    public boolean equals(Axis anAxis) {
        if (anAxis == null) {
            return false;
        }

        if (anAxis.isSpectrum() != spectrum) {
            return false;
        }

        return axisEquals(anAxis);

    }

    public boolean axisEquals(Axis anAxis) {
        if (anAxis == null) {
            return false;
        }

        String tmpAxis = anAxis.getAxis();
        if (tmpAxis == null && axis == null) {
            return true;
        }

        if (tmpAxis == null ^ axis == null) {
            return false;
        }

        if (tmpAxis.equals(axis)) {
            return true;
        }

        return false;
    }

    public Axis clone() {
        Axis copy = new Axis();
        copy.setSpectrum(this.isSpectrum());
        copy.setAxis(this.getAxis());
        return copy;
    }

}
