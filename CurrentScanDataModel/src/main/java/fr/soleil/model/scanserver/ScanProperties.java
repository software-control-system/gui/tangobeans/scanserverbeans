package fr.soleil.model.scanserver;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.comete.definition.widget.properties.ChartProperties;

public class ScanProperties {

    private ChartProperties chartProperties = null;

    private String runName = null;

    private List<String> y1AttributeList = new ArrayList<String>();

    private List<String> y2AttributeList = new ArrayList<String>();

    private List<String> zAttributeList = new ArrayList<String>();

    private String xAttribute = null;

    public ScanProperties(String runName) {
        this.runName = runName;
    }

    public String getXAttribute() {
        return xAttribute;
    }

    public void setXAttribute(String xAttribute) {
        this.xAttribute = xAttribute;
    }

    public ChartProperties getChartProperties() {
        return chartProperties;
    }

    public void setChartProperties(ChartProperties chartProperties) {
        this.chartProperties = chartProperties;
    }

    public String getRunName() {
        return runName;
    }

    public List<String> getY1AttributeList() {
        return y1AttributeList;
    }

    public void setY1AttributeList(List<String> y1AttributeList) {
        this.y1AttributeList.clear();
        if (y1AttributeList != null) {
            this.y1AttributeList.addAll(y1AttributeList);
        }
    }

    public List<String> getY2AttributeList() {
        return y2AttributeList;
    }

    public void setY2AttributeList(List<String> y2AttributeList) {
        this.y2AttributeList.clear();
        if (y2AttributeList != null) {
            this.y2AttributeList.addAll(y2AttributeList);
        }
    }

    public List<String> getZAttributeList() {
        return zAttributeList;
    }

    public void setZAttributeList(List<String> zAttributeList) {
        this.zAttributeList.clear();
        if (zAttributeList != null) {
            this.zAttributeList.addAll(zAttributeList);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("runName=" + runName + "\n");
        builder.append("xAttribute=" + xAttribute + "\n");
        builder.append("y1AttributeList=" + y1AttributeList + "\n");
        builder.append("y2AttributeList=" + y2AttributeList + "\n");
        builder.append("zAttributeList=" + zAttributeList + "\n");
        builder.append("chartProperties=" + chartProperties.getBackgroundColor());
        return builder.toString();
    }

}
