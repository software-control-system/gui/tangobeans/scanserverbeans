package fr.soleil.model.scanserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class CurrentConfigurationParser {

    /**
     * Read scanServerName/runName attribute
     */
    public static String getConfigurationName(String scanServerName) {
        String runName = "no configuration";

        if (TangoAttributeHelper.isAttributeRunning(scanServerName, CurrentScanDataModel.RUN_NAME)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(CurrentScanDataModel.RUN_NAME);
                    runName = attribute.extractString();

                    if (runName.equalsIgnoreCase("unamed")) {
                        runName = "SalsaConfiguration";
                    }
                } catch (DevFailed e) {
                }
            }
        }
        return runName;
    }

    /**
     * Read the current trajectories defined
     */
    public static List<Trajectory> getCurrentTrajectory(String scanServerName, int index) throws Exception {

        List<Trajectory> trajectoryList = new ArrayList<>();
        String indexStr = String.valueOf(index);
        if (index == 1) {
            indexStr = "";
        }

        String[] actuators = readCurrentActuators(scanServerName, index);
        if (actuators != null && actuators.length > 0) {
            int nbActuator = actuators.length;
            String trajectoriesName = CurrentScanDataModel.TRAJECTORIES + indexStr;
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                DeviceAttribute attribute = proxy.read_attribute(trajectoriesName);
                double[] allValues = attribute.extractDoubleArray();
                if (allValues != null && allValues.length > 0) {
                    double[] flatValues = Arrays.copyOf(allValues, attribute.getNbRead());
                    int nbPoints = flatValues.length / nbActuator;
                    double[] line = null;
                    Trajectory trajectory = null;
                    for (int i = 0; i < nbActuator; i++) {
                        line = new double[nbPoints];
                        System.arraycopy(flatValues, i * nbPoints, line, 0, nbPoints);
                        trajectory = new Trajectory();
                        trajectory.setCompleteTrajectory(line);
                        trajectory.setActuator(actuators[i]);
                        trajectoryList.add(trajectory);
                    }
                }
            }
        }
        return trajectoryList;
    }

    public static double[] readFlatTrajectory(String scanServerName, int index) {
        double[] flatValues = null;
        String indexStr = String.valueOf(index);
        if (index == 1) {
            indexStr = "";
        }

        String trajectoriesName = CurrentScanDataModel.TRAJECTORIES + indexStr;

        if (TangoAttributeHelper.isAttributeRunning(scanServerName, trajectoriesName)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(trajectoriesName);
                    double[] allValues = attribute.extractDoubleArray();
                    if (allValues != null && allValues.length > 0) {
                        flatValues = Arrays.copyOf(allValues, attribute.getNbRead());
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        }

        return flatValues;
    }

    public static String[] readCurrentSensors(String scanServerName) {
        return CurrentScanDataModel.readAttributeList(scanServerName, CurrentScanDataModel.SENSORS_LIST);
    }

    public static String[] readCurrentTimeBases(String scanServerName) {
        return CurrentScanDataModel.readAttributeList(scanServerName, CurrentScanDataModel.TIMEBASES);
    }

    public static String[] readCurrentActuators(String scanServerName, int index) {
        String indexStr = String.valueOf(index);
        if (index == 1) {
            indexStr = "";
        }
        return CurrentScanDataModel.readAttributeList(scanServerName, CurrentScanDataModel.ACTUATORS_LIST + indexStr);
    }

    public static double readActuatorDelay(String scanServerName) {
        return readDouble(scanServerName, CurrentScanDataModel.ACTUATOR_DELAY);
    }

    public static double[] readIntegrationTime(String scanServerName) {
        double[] integrationTime = null;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, CurrentScanDataModel.INTEGRATION_TIMES)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(CurrentScanDataModel.INTEGRATION_TIMES);
                    integrationTime = attribute.extractDoubleArray();
                } catch (DevFailed e) {
                    //NOP
                }
            }
        }
        return integrationTime;
    }

    public static String readContextValidation(String scanServerName) {
        return CurrentScanDataModel.readStringAttribute(scanServerName, CurrentScanDataModel.CONTEXT_VALIDATION);
    }

    public static int readErrorStrategy(String scanServerName, String cat) {
        return readInt(scanServerName, cat + CurrentScanDataModel.ERROR_STRATEGY);
    }

    public static double readRetryTimeOut(String scanServerName, String cat) {
        return readDouble(scanServerName, cat + CurrentScanDataModel.RETRY_TIMEOUT);
    }

    public static int readRetryCount(String scanServerName, String cat) {
        return readInt(scanServerName, cat + CurrentScanDataModel.RETRY_COUNT);
    }

    public static double readTimeOut(String scanServerName, String cat) {
        return readDouble(scanServerName, cat + CurrentScanDataModel.TIMEOUT);
    }

    public static int readScanNumber(String scanServerName) {
        return readInt(scanServerName, CurrentScanDataModel.SCAN_NUMBER);
    }

    public static boolean isOnTheFly(String scanServerName) {
        return readBoolean(scanServerName, CurrentScanDataModel.ON_THE_FLY);
    }

    public static boolean isZigZag(String scanServerName) {
        return readBoolean(scanServerName, CurrentScanDataModel.ZIGZAG);
    }

    public static boolean isHCS(String scanServerName) {
        return readBoolean(scanServerName, CurrentScanDataModel.HCS);
    }

    public static boolean isEnableScanSpeed(String scanServerName) {
        return readBoolean(scanServerName, CurrentScanDataModel.ENABLESCANSPEED);
    }

    public static boolean is2DScan(String scanServerName) {
        String[] values = readCurrentActuators(scanServerName, 2);
        return (values != null && values.length > 0);
    }

    public static boolean isEnergy(String scanServerName) {
        boolean isEnergy = false;
        // String[] actuators = CurrentScanDataModel.readAttributeList(scanServerName,
        // CurrentScanDataModel.ACTUATORS_LIST);
        // // Energy is only for 1D scan
        // if (actuators != null && actuators.length > 0) {
        // for (String actuatorName : actuators) {
        // if (actuatorName.toLowerCase().endsWith("energy")) {
        // isEnergy = true;
        // break;
        // }
        // }
        // }
        return isEnergy;
    }

    private static boolean readBoolean(String scanServerName, String attributeName) {
        boolean result = false;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(attributeName);
                    result = attribute.extractBoolean();
                } catch (DevFailed e) {
                }
            }
        }
        return result;
    }

    private static double readDouble(String scanServerName, String attributeName) {
        double result = Double.NaN;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(attributeName);
                    result = attribute.extractDouble();
                } catch (DevFailed e) {
                }
            }
        }
        return result;
    }

    private static int readInt(String scanServerName, String attributeName) {
        int result = 0;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName);
            if (proxy != null) {
                try {
                    DeviceAttribute attribute = proxy.read_attribute(attributeName);
                    result = attribute.extractLong();
                } catch (DevFailed e) {
                }
            }
        }
        return result;
    }
}
