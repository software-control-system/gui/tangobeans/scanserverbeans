package fr.soleil.model.scanserver;

public class Action {

    // Map on afterRunActionType scan server attribute
    private final static int NO_ACTION = 0;
    private final static int FIRST_ACTION = 1;
    private final static int BEFORE_ACTION = 2;
    private final static int MAX_ACTION = 3;
    private final static int MIN_ACTION = 4;
    private final static int MAX_RELATIVE_ACTION = 5;
    private final static int MIN_RELATIVE_ACTION = 6;
    private final static int CENTER_MASS_ACTION = 7;

    // Label for afterRunActionType
    private final static String TEXT_NO_ACTION = "No operation";
    private final static String TEXT_FIRST_ACTION = "Scan 1st position";
    private final static String TEXT_BEFORE_ACTION = "Before scan";
    private final static String TEXT_MAX_ACTION = "Sensor max";
    private final static String TEXT_MIN_ACTION = "Sensor min";
    private final static String TEXT_MAX_RELATIVE_ACTION = "Max of derivative";
    private final static String TEXT_MIN_RELATIVE_ACTION = "Min of derivative";
    private final static String TEXT_CENTER_MASS_ACTION = "Center of mass";

    // Description of afterRunActionType
    private final static String DESC_NO_ACTION = "Nothing to do";
    private final static String DESC_FIRST_ACTION = "Go to scan first position";
    private final static String DESC_BEFORE_ACTION = "Go to position before scan";
    private final static String DESC_MAX_ACTION = "Go to the maximum position of a sensor";
    private final static String DESC_MIN_ACTION = "Go to the minimum position of a sensor";
    private final static String DESC_MAX_RELATIVE_ACTION = "Go to the position which maximises the derivative of a sensor relative to an actuator";
    private final static String DESC_MIN_RELATIVE_ACTION = "Go to the position which minimises the derivative of a sensor relative to an actuator";
    private final static String DESC_CENTER_MASS_ACTION = "Go to the center of mass of a sensor relative to an actuator";

    // If afterRunActionSensor must be defined for associated afterRunActionType
    private final static boolean NO_ACTION_SENSOR = false;
    private final static boolean FIRST_ACTION_SENSOR = false;
    private final static boolean BEFORE_ACTION_SENSOR = false;
    private final static boolean MAX_ACTION_SENSOR = true;
    private final static boolean MIN_ACTION_SENSOR = true;
    private final static boolean MAX_RELATIVE_ACTION_SENSOR = true;
    private final static boolean MIN_RELATIVE_ACTION_SENSOR = true;
    private final static boolean CENTER_MASS_ACTION_SENSOR = true;

    // If afterRunActionActuator must be defined for associated afterRunActionType
    private final static boolean NO_ACTION_ACTUATOR = false;
    private final static boolean FIRST_ACTION_ACTUATOR = false;
    private final static boolean BEFORE_ACTION_ACTUATOR = false;
    private final static boolean MAX_ACTION_ACTUATOR = false;
    private final static boolean MIN_ACTION_ACTUATOR = false;
    private final static boolean MAX_RELATIVE_ACTION_ACTUATOR = true;
    private final static boolean MIN_RELATIVE_ACTION_ACTUATOR = true;
    private final static boolean CENTER_MASS_ACTION_ACTUATOR = true;

    public final static Action ACTION_0 = new Action(NO_ACTION, TEXT_NO_ACTION, DESC_NO_ACTION,
            NO_ACTION_SENSOR, NO_ACTION_ACTUATOR);
    public final static Action ACTION_1 = new Action(FIRST_ACTION, TEXT_FIRST_ACTION,
            DESC_FIRST_ACTION, FIRST_ACTION_SENSOR, FIRST_ACTION_ACTUATOR);
    public final static Action ACTION_2 = new Action(BEFORE_ACTION, TEXT_BEFORE_ACTION,
            DESC_BEFORE_ACTION, BEFORE_ACTION_SENSOR, BEFORE_ACTION_ACTUATOR);
    public final static Action ACTION_3 = new Action(MAX_ACTION, TEXT_MAX_ACTION, DESC_MAX_ACTION,
            MAX_ACTION_SENSOR, MAX_ACTION_ACTUATOR);
    public final static Action ACTION_4 = new Action(MIN_ACTION, TEXT_MIN_ACTION, DESC_MIN_ACTION,
            MIN_ACTION_SENSOR, MIN_ACTION_ACTUATOR);
    public final static Action ACTION_5 = new Action(MAX_RELATIVE_ACTION, TEXT_MAX_RELATIVE_ACTION,
            DESC_MAX_RELATIVE_ACTION, MAX_RELATIVE_ACTION_SENSOR, MAX_RELATIVE_ACTION_ACTUATOR);
    public final static Action ACTION_6 = new Action(MIN_RELATIVE_ACTION, TEXT_MIN_RELATIVE_ACTION,
            DESC_MIN_RELATIVE_ACTION, MIN_RELATIVE_ACTION_SENSOR, MIN_RELATIVE_ACTION_ACTUATOR);
    public final static Action ACTION_7 = new Action(CENTER_MASS_ACTION, TEXT_CENTER_MASS_ACTION,
            DESC_CENTER_MASS_ACTION, CENTER_MASS_ACTION_SENSOR, CENTER_MASS_ACTION_ACTUATOR);

    public final static Action[] actionArray = new Action[] { ACTION_0, ACTION_1, ACTION_2,
            ACTION_3, ACTION_4, ACTION_5, ACTION_6, ACTION_7 };

    public final static String[] labelArray = new String[] { ACTION_0.getActionLabel(),
            ACTION_1.getActionLabel(), ACTION_2.getActionLabel(), ACTION_3.getActionLabel(),
            ACTION_4.getActionLabel(), ACTION_5.getActionLabel(), ACTION_6.getActionLabel(),
            ACTION_7.getActionLabel() };

    public final static String[] valueArray = new String[] {
            String.valueOf(ACTION_0.getActionType()), String.valueOf(ACTION_1.getActionType()),
            String.valueOf(ACTION_2.getActionType()), String.valueOf(ACTION_3.getActionType()),
            String.valueOf(ACTION_4.getActionType()), String.valueOf(ACTION_5.getActionType()),
            String.valueOf(ACTION_6.getActionType()), String.valueOf(ACTION_7.getActionType()) };

    private int actionType = NO_ACTION;
    private String actionLabel = TEXT_NO_ACTION;
    private String actionDescription = DESC_NO_ACTION;
    private boolean sensorRequired = false;
    private boolean actuatorRequired = false;

    public Action(int actionType, String actionLabel, String actionDescription,
            boolean sensorRequired, boolean actuatorRequired) {
        super();
        this.actionType = actionType;
        this.actionLabel = actionLabel;
        this.actionDescription = actionDescription;
        this.sensorRequired = sensorRequired;
        this.actuatorRequired = actuatorRequired;
    }

    public int getActionType() {
        return actionType;
    }

    public String getActionLabel() {
        return actionLabel;
    }

    public String getActionDescription() {
        return actionDescription;
    }

    public boolean isSensorRequired() {
        return sensorRequired;
    }

    public boolean isActuatorRequired() {
        return actuatorRequired;
    }

    public static Action getAction(int index) {
        if (index >= 0 && index < actionArray.length) {
            return actionArray[index];
        }
        return null;
    }

}
