package fr.soleil.model.scanserver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.activation.UnsupportedDataTypeException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.StateUtilities;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoConstHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.comete.tango.data.service.helper.TangoExceptionHelper;
import fr.soleil.data.container.matrix.DoubleMatrix;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.tango.clientapi.TangoAttribute;

public class CurrentScanDataModel {

    public static final Logger LOGGER = LoggerFactory.getLogger(CurrentScanDataModel.class);

    // ATTRIBUTES
    public static final String SCAN_TYPE = "scanType";
    public static final String RUN_NAME = "runName";
    public static final String SCAN_NUMBER = "scanNumber";
    public static final String ACTUATOR_DELAY = "actuatorsDelay";
    public static final String TIMEBASE_DELAY = "timebasesDelay";
    public static final String SENSORS_TIMESTAMPS = "sensorsTimestamps";
    public static final String ACTUATORS_TIMESTAMPS = "actuatorsTimestamps";
    public static final String SENSORS_DATA_LIST = "sensorsDataList";
    public static final String SENSORS_LIST = "sensors";
    public static final String ACTUATORS_DATA_LIST = "actuatorsDataList";
    public static final String ACTUATORS_LIST = "actuators";
    public static final String YACTUATORS_DATA_LIST = "actuators2DataList";
    public static final String TIMEBASES = "timebases";
    public static final String YACTUATORS_LIST = "actuators2";
    public static final String INTEGRATION_TIMES = "integrationTimes";
    public static final String HISTORIC = "historic";
    public final static String DEAD_TIME_PERCENT = "deadTimePercent";
    public final static String DEAD_TIME = "deadTime";
    public final static String DEAD_TIME_PER_POINT = "deadTimePerPoint";
    public final static String ON_THE_FLY = "onTheFly";
    public final static String ZIGZAG = "zigzag";
    public final static String HCS = "hwContinuous";
    public final static String ENABLESCANSPEED = "enableScanSpeed";
    public final static String CONTEXT_VALIDATION = "contextValidation";
    public final static String CONTEXT_VALIDATION_STRATEGY = "contextValidationErrorStrategy";
    public final static String TRAJECTORIES = "trajectories";
    public final static String SCAN_INFO = "scanInfo";
    public final static String ERROR_STRATEGY = "ErrorStrategy";
    public final static String RETRY_TIMEOUT = "RetryTimeOut";
    public final static String RETRY_COUNT = "RetryCount";
    public final static String TIMEOUT = "TimeOut";
    public final static String AFTER_ACTION_TYPE = "afterRunActionType";
    public final static String AFTER_ACTION_SENSOR = "afterRunActionSensor";
    public final static String AFTER_ACTION_ACTUATOR = "afterRunActionActuator";
    private static final String TIMESTAMPS = "Timestamps";
    private static final String DATA = "data";

    private static final String SENSOR = "(sensor)";
    private static final String THOUSAND = "1000";

    // SCAN INFO
    public final static String SCAN_STATE = "State";
    public final static String SCAN_STATUS = "Status";
    public final static String SCAN_START_DATE = "scanStartDate";
    public final static String SCAN_END_DATE = "scanEndDate";
    public final static String SCAN_DURATION = "scanDuration";
    public final static String SCAN_REMAINING_TIME = "scanRemainingTime";
    public final static String SCAN_COMPLETION = "scanCompletion";
    public final static String SCAN_ELAPSED = "scanElapsed";

    // COMMAND

    // HOOK
    public final static String PRE_RUN_HOOK = "preRunHooks";
    public final static String PRE_SCAN_HOOK = "preScanHooks";
    public final static String PRE_STEP_HOOK = "preStepHooks";
    public final static String POST_ACTUATOR_HOOK = "postActuatorMoveHooks";
    public final static String POST_INTEGRATION_HOOK = "postIntegrationHooks";
    public final static String POST_STEP_HOOK = "postStepHooks";
    public final static String POST_SCAN_HOOK = "postScanHooks";
    public final static String POST_RUN_HOOK = "postRunHooks";

    // NEXUS
    public final static String NEXUS_FILE = "generatedNexusFileName";
    public final static String DATA_RECORDER = "DataRecorder";
    public final static String DATA_RECORDED = "dataRecorded";
    public final static String DEFAULT_RECORDER = "storage/recorder/datarecorder.1";
    public final static String NX_ENTRY = "nxEntryName";
    public final static String DATA_RECORDER_PARTIAL_MODE = "dataRecorderPartialMode";
    public final static String DATA_RECORDER_CONFIG = "dataRecorderConfig";

    // RUN INFO
    public final static String RUN_START_DATE = "runStartDate";
    public final static String RUN_END_DATE = "runEndDate";
    public final static String RUN_DURATION = "runDuration";
    public final static String RUN_REMAINING_TIME = "runRemainingTime";
    public final static String RUN_ELAPSED = "runElapsed";
    public final static String RUN_COMPLETION = "runCompletion";

    // ACTION
    public final static String EXECUTE_ACTION = "ExecuteAction";
    public final static String ACTUATOR_ACTION = "afterRunActionActuator";
    public final static String SENSOR_ACTION = "afterRunActionSensor";
    public final static String TYPE_ACTION = "afterRunActionType";

    // ERRORS AND LOGS
    private static final String CANNOT_FIRE_STATE_CHANGED = "Cannot fireStateChanged {}";
    private static final String STACK_TRACE = "Stack trace";
    private static final String CANNOT_READ = "Cannot read {}/{} {}";
    private static final String CANNOT_EXECUTE_STATE = "Cannot execute {}/State {}";
    private static final String CANNOT_EXECUTE_STATUS = "Cannot execute {}/Status {}";
    private static final String DATA_01_IS_NOT_CREATED_YET = "{}/data_01 is not created yet";
    private static final String CANNOT_SET_INTO_MATRIX = "Cannot set {}  into matrix {}";
    private static final String CANNOT_WRITE_DISPLAY_UNIT_1000 = "Cannot write {}/{}/display_unit=1000 {}";
    private static final String CANNOT_READ_ATTRIBUTE_RETURNED_ATTRIBUTE_IS_NULL_TRY_NB = "Cannot read_attribute {}/{}: returned attribute is null! (try nb {})";
    private static final String ATTRIBUTE_HAS_AN_EMPTY_VALUE_TRY_NB = "Attribute {}/{} has an empty value! (try nb {})";
    private static final String VALUE_EQUALS = "{}/{}={}";
    private static final String CANNOT_READ_ATTRIBUTE_N_TRY_NB = "Cannot read_attribute {}/{} {}\\n(try nb{})";
    private static final String NO_NEXUS_FILE_GENERATED_IT_TOOK_TOO_MUCH_TIME = "No nexus file generated: it took too much time to try to read {}/{}";
    private static final String NO_NEXUS_FILE_GENERATED = "No nexus file generated";
    private static final String CANNOT_UPDATE_NEXUS_INFO = "Cannot updateNexusInfo {}";
    private static final String ALL = "_ALL";

    private static final Map<String, Map<String, ScanServerDataModel>> SCAN_SERVER_DATA_MODEL_MAP = new HashMap<>();
    private static final Map<String, ScanServerStateRefresher> STATE_MEDIATOR_MAP = new HashMap<>();
    private static final Map<String, List<IScanServerListener>> LISTENER_MAP = new HashMap<>();
    private static final Map<String, String> DATE_MAP = new HashMap<>();

    // Map<Attribute Label, attributeName>
    private static final Map<String, String> ATT_NAME = new HashMap<>();
    // Map<AttributeName, attributeValues>
    private static final Map<String, String[]> ATT_STRING_SPECTRUM_VALUE = new HashMap<>();
    // Map<ScanServerName, nexusFileName>
    private static final Map<String, String> NEXUS_FILE_NAME_MAP = new HashMap<>();
    // Map<ScanServerName, nexusFileEntry>
    private static final Map<String, String> NEXUS_FILE_ENTRY_MAP = new HashMap<>();
    // Map<ScanServerName, runName>
    private static final Map<String, String> RUN_NAME_MAP = new HashMap<>();
    // Map<runName, CurrentScanProperties>
    private static final Map<String, ScanProperties> SCAN_PROPERTIES_MAP = new HashMap<>();

    // Whether accesses to scanserver device should be synchronized.
    // true by default (CONTROLGUI-386, PROBLEM-2255).
    // Can be changed though setSynchronizeDeviceAccesses(boolean) (CONTROLGUI-389, PROBLEM-2297)
    private static boolean synchronizeDeviceAccesses = true;

    /**
     * Returns whether accesses to scanserver device are synchronized.
     * 
     * @return A <code>boolean</code>.
     */
    public static boolean isSynchronizeDeviceAccesses() {
        return synchronizeDeviceAccesses;
    }

    /**
     * Sets whether accesses to scanserver device should be synchronized.
     * 
     * @param synchronizeDeviceAccesses Whether accesses to scanserver device should be synchronized.
     */
    public static void setSynchronizeDeviceAccesses(boolean synchronizeDeviceAccesses) {
        CurrentScanDataModel.synchronizeDeviceAccesses = synchronizeDeviceAccesses;
    }

    public static Map<String, ScanProperties> getScanPropertiesMap() {
        return SCAN_PROPERTIES_MAP;
    }

    public static void setScanPropertiesMap(Map<String, ScanProperties> map) {
        if (map != null) {
            SCAN_PROPERTIES_MAP.putAll(map);
        }
    }

    private static ScanProperties getCurrentScanProperties(String scanServerName, boolean buildProp) {
        String runName = readRunName(scanServerName);
        return getScanProperties(scanServerName, runName, buildProp);
    }

    private static ScanProperties getScanProperties(String scanServerName, String runName, boolean buildProp) {
        ScanProperties prop = null;
        if (runName != null) {
            prop = SCAN_PROPERTIES_MAP.get(runName);
            if (prop == null) {
                prop = new ScanProperties(runName);
                SCAN_PROPERTIES_MAP.put(runName, prop);
            }

            if ((prop != null) && buildProp) {
                prop.setY1AttributeList(buildAttributeAxisList(scanServerName, Axis.Y1));
                prop.setY2AttributeList(buildAttributeAxisList(scanServerName, Axis.Y2));
                prop.setZAttributeList(buildAttributeAxisList(scanServerName, Axis.Z));
                prop.setXAttribute(getActuatorName(scanServerName));
            }
        }
        return prop;
    }

    public static ChartProperties getChartPropertyScan(String scanServerName) {
        ScanProperties prop = getCurrentScanProperties(scanServerName, false);
        ChartProperties chartProperties = null;
        if (prop != null) {
            chartProperties = prop.getChartProperties();
        }
        return chartProperties;
    }

    public static void setChartPropertyScan(String scanServerName, ChartProperties properties) {
        ScanProperties prop = getCurrentScanProperties(scanServerName, false);
        if (prop != null) {
            prop.setChartProperties(properties);
        }
    }

    public static boolean dateChanged(String scanServerName, String oldDate) {
        String newDate = getStartDate(scanServerName);
        boolean dateChanged = false;
        if ((newDate != null) && ((oldDate == null) || !oldDate.equals(newDate))) {
            dateChanged = true;
        }
        return dateChanged;
    }

    public static void addScanServerListener(String scanServerName, IScanServerListener listener) {
        if ((listener != null) && (scanServerName != null) && !scanServerName.isEmpty()) {
            scanServerName = scanServerName.toLowerCase();
            List<IScanServerListener> listenerList = LISTENER_MAP.get(scanServerName);
            if (listenerList == null) {
                listenerList = new ArrayList<>();
                LISTENER_MAP.put(scanServerName, listenerList);
            }

            if (!listenerList.contains(listener)) {
                listenerList.add(listener);
            }

            if (!STATE_MEDIATOR_MAP.containsKey(scanServerName)) {
                ScanServerStateRefresher thread = new ScanServerStateRefresher(scanServerName);
                STATE_MEDIATOR_MAP.put(scanServerName, thread);
                thread.start();
            }
        }
    }

    public static void removeScanServerListener(String scanServerName, IScanServerListener listener) {
        if ((listener != null) && (scanServerName != null) && !scanServerName.isEmpty()) {
            scanServerName = scanServerName.toLowerCase();
            List<IScanServerListener> listenerList = LISTENER_MAP.get(scanServerName);
            if ((listenerList != null) && listenerList.contains(listener)) {
                listenerList.remove(listener);
                if (listenerList.size() == 0) {
                    LISTENER_MAP.remove(scanServerName);
                    listenerList = null;
                    ScanServerStateRefresher thread = STATE_MEDIATOR_MAP.get(scanServerName);
                    if ((thread != null) && thread.isAlive()) {
                        thread.interrupt();
                        thread = null;
                    }
                }
            }
        }
    }

    public static void fireStateChanged(String scanServerName, String state) {
        try {
            if ((scanServerName != null) && !scanServerName.isEmpty()) {
                scanServerName = scanServerName.toLowerCase();
                List<IScanServerListener> listenerList = LISTENER_MAP.get(scanServerName);
                if (listenerList != null) {
                    // Copy the list before for avoiding concurrent modification
                    List<IScanServerListener> copyListenerList = new ArrayList<>();
                    copyListenerList.addAll(listenerList);
                    for (IScanServerListener listener : copyListenerList) {
                        listener.stateChanged(state);
                    }
                }
            }
        } catch (Exception excp) {
            LOGGER.error(CANNOT_FIRE_STATE_CHANGED, excp.getMessage());
            LOGGER.debug(STACK_TRACE, excp);
        }
    }

    protected static int getAttributeHeightNoLock(DeviceProxy proxy, String attributeName) throws DevFailed {
        int height;
        DeviceAttribute deviceAttribute = proxy.read_attribute(attributeName);
        if (deviceAttribute == null) {
            height = 0;
        } else {
            height = deviceAttribute.getDimY();
        }
        return height;
    }

    public static int getAttributeHeight(String scanServerName, String attributeName) {
        int height = 0;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName)) {
            int type = TangoAttributeHelper.getAttributeType(scanServerName, attributeName);
            if (type == TangoConstHelper.IMAGE_TYPE) {
                try {
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
                    if (proxy != null) {
                        if (isSynchronizeDeviceAccesses()) {
                            synchronized (proxy) {
                                height = getAttributeHeightNoLock(proxy, attributeName);
                            }
                        } else {
                            height = getAttributeHeightNoLock(proxy, attributeName);
                        }
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerName, attributeName, TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            } else {
                height = 1;
            }
        }

        return height;
    }

    protected static int getAttributeWidthNoLock(DeviceProxy proxy, String attributeName) throws DevFailed {
        int width;
        DeviceAttribute deviceAttribute = proxy.read_attribute(attributeName);
        if (deviceAttribute == null) {
            width = 0;
        } else {
            width = deviceAttribute.getDimX();
        }
        return width;
    }

    public static int getAttributeWidth(String scanServerName, String attributeName) {
        int width = 0;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName)) {
            int type = TangoAttributeHelper.getAttributeType(scanServerName, attributeName);
            if (type == TangoConstHelper.IMAGE_TYPE) {
                try {
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
                    if (proxy != null) {
                        if (isSynchronizeDeviceAccesses()) {
                            synchronized (proxy) {
                                width = getAttributeWidthNoLock(proxy, attributeName);
                            }
                        } else {
                            width = getAttributeWidthNoLock(proxy, attributeName);
                        }
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerName, attributeName, TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            } else {
                width = 1;
            }
        }

        return width;
    }

    protected static String getStateNoLock(DeviceProxy proxy) throws DevFailed {
        DevState devState = proxy.state();
        return StateUtilities.getNameForState(devState);
    }

    public static String getState(String scanServerName) {
        String state = StateUtilities.getNameForState(DevState.UNKNOWN);
        // Do not test if the device is running, it is overload the database
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        if (proxy != null) {
            try {
                if (isSynchronizeDeviceAccesses()) {
                    synchronized (proxy) {
                        state = getStateNoLock(proxy);
                    }
                } else {
                    state = getStateNoLock(proxy);
                }
            } catch (Exception e) {
                LOGGER.error(CANNOT_EXECUTE_STATE, scanServerName, TangoExceptionHelper.getErrorMessage(e));
                LOGGER.debug(STACK_TRACE, e);
            }
        }
        return state;
    }

    protected static int getScanTypeNoLock(DeviceProxy proxy) throws DevFailed {
        DeviceAttribute deviceAttribute = proxy.read_attribute(SCAN_TYPE);
        return deviceAttribute.extractLong();
    }

    public static int getScanType(String scanServerName) {
        int value = -1;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, SCAN_TYPE)) {
            final DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
            if (proxy != null) {
                try {
                    if (isSynchronizeDeviceAccesses()) {
                        synchronized (proxy) {
                            value = getScanTypeNoLock(proxy);
                        }
                    } else {
                        value = getScanTypeNoLock(proxy);
                    }
                } catch (final DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerName, SCAN_TYPE, TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            }
        }
        return value;
    }

    protected static String getStatusNoLock(DeviceProxy proxy) throws DevFailed {
        return proxy.status();
    }

    public static String getStatus(String scanServerName) {
        String status = ObjectUtils.EMPTY_STRING;
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
        try {
            if (proxy != null) {
                if (isSynchronizeDeviceAccesses()) {
                    synchronized (proxy) {
                        status = getStatusNoLock(proxy);
                    }
                } else {
                    status = getStatusNoLock(proxy);
                }
            }
        } catch (DevFailed e) {
            LOGGER.error(CANNOT_EXECUTE_STATUS, scanServerName, TangoExceptionHelper.getErrorMessage(e));
            LOGGER.debug(STACK_TRACE, e);
        }
        return status;
    }

    public static boolean isMoving(String state) {
        boolean isMoving = false;
        if (state != null) {
            if (state.equals(StateUtilities.getNameForState(DevState.MOVING))) {
                isMoving = true;
            }
        }
        return isMoving;
    }

    public static boolean isScanResultReady(String scanServerName) {
        boolean isDataCreated = TangoAttributeHelper.isAttributeRunning(scanServerName, "data_01");
        LOGGER.trace(DATA_01_IS_NOT_CREATED_YET, scanServerName);
        return isDataCreated;
    }

    public static double getValueAtIndex(String scanServerDeviceName, String attributeName, int x, int y) {
        double value = Double.NaN;
        if (TangoAttributeHelper.isAttributeRunning(scanServerDeviceName, attributeName)) {
            if (isSpectrum(scanServerDeviceName, attributeName)) {
                // If it is a spectrum we search the YActuator element
                value = getValueAtIndex(scanServerDeviceName, attributeName, y);
            } else if ((x >= 0) && (y >= 0)) {
                double[] doubleValues = null;
                try {
                    TangoAttribute tangoAttribute = new TangoAttribute(
                            scanServerDeviceName + TangoDeviceHelper.SLASH + attributeName);
                    doubleValues = (double[]) tangoAttribute.readArray(Double.TYPE);
                    int dimX = tangoAttribute.getDimX();
                    int dimY = tangoAttribute.getDimY();
                    if ((doubleValues != null) && (x < dimX) && (y < dimY)) {
                        DoubleMatrix matrix = new DoubleMatrix();
                        matrix.setFlatValue(doubleValues, dimY, dimX);
                        value = matrix.getValueAt(y, x);
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerDeviceName, attributeName,
                            TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                } catch (UnsupportedDataTypeException e) {
                    LOGGER.error(CANNOT_SET_INTO_MATRIX, Arrays.toString(doubleValues), e.getMessage());
                    LOGGER.debug(STACK_TRACE, e);
                }
            }
        }

        return value;

    }

    public static double getValueAtIndex(String scanServerDeviceName, String attributeName, int index) {
        double value = Double.NaN;
        if (index >= 0) {
            if (TangoAttributeHelper.isAttributeRunning(scanServerDeviceName, attributeName)) {
                try {
                    TangoAttribute tangoAttribute = new TangoAttribute(
                            scanServerDeviceName + TangoDeviceHelper.SLASH + attributeName);
                    double[] doubleValues = (double[]) tangoAttribute.readArray(Double.TYPE);
                    if ((doubleValues != null) && (index < doubleValues.length)) {
                        value = doubleValues[index];
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerDeviceName, attributeName,
                            TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            }
        }
        return value;

    }

    public static ScanServerDataModel getDataModel(String scanServerName, String attributeName, boolean createModel) {
        ScanServerDataModel dataModel = null;
        if ((scanServerName != null) && !scanServerName.isEmpty() && (attributeName != null)
                && !attributeName.isEmpty()) {
            Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
            if (map != null) {
                dataModel = map.get(attributeName);
                if ((dataModel == null) && createModel) {
                    dataModel = new ScanServerDataModel(attributeName.toLowerCase());
                    map.put(attributeName.toLowerCase(), dataModel);
                }
            }
        }
        return dataModel;
    }

    public static ScanServerDataModel getDataModel(String scanServerName, String attributeName) {
        return getDataModel(scanServerName, attributeName, false);
    }

    public static ScanServerDataModel getDataModel(String scanServerName, int index) {
        ScanServerDataModel dataModel = null;
        if ((index > -1) && (scanServerName != null) && !scanServerName.isEmpty()) {
            Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
            if (map != null) {
                if (index < map.size()) {
                    Set<String> keySet = map.keySet();
                    String[] keysArray = trimArrayAndSort(keySet.toArray(new String[keySet.size()]));
                    if (keysArray != null) {
                        dataModel = map.get(keysArray[index]);
                    }
                }
            }
        }
        return dataModel;
    }

    private static boolean dateChanged(String scanServerName) {
        boolean dateChanged = false;
        if ((scanServerName != null) && !scanServerName.isEmpty()) {
            String oldDate = DATE_MAP.get(scanServerName.toLowerCase());
            if (dateChanged(scanServerName, oldDate)) {
                dateChanged = true;
                String newDate = getStartDate(scanServerName);
                DATE_MAP.put(scanServerName.toLowerCase(), newDate);
            }
        }
        return dateChanged;

    }

    private static void clearDataModelMap(String scanServerName) {
        Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
        if (map != null) {
            map.clear();
        }
    }

    public static String getStartDate(String aScanServerDeviceName) {
        return readStringAttribute(aScanServerDeviceName, RUN_START_DATE);
    }

    public static String readRunName(String scanServerDeviceName) {
        return readRunName(scanServerDeviceName, true);
    }

    public static String readRunName(String scanServerDeviceName, boolean forceUpdate) {
        String runName = null;
        if ((scanServerDeviceName != null) && !scanServerDeviceName.isEmpty()) {
            runName = RUN_NAME_MAP.get(scanServerDeviceName.toLowerCase());
            if ((runName == null) || forceUpdate) {
                runName = readStringAttribute(scanServerDeviceName, RUN_NAME);
                RUN_NAME_MAP.put(scanServerDeviceName.toLowerCase(), runName);
            }
        }
        return runName;
    }

    protected static String readStringAttributeNoLock(DeviceProxy proxy, String attributeName) throws DevFailed {
        DeviceAttribute deviceAttribute = proxy.read_attribute(attributeName);
        return deviceAttribute.extractString();
    }

    public static String readStringAttribute(String scanServerDeviceName, String attributeName) {
        String value = null;
        if (TangoAttributeHelper.isAttributeRunning(scanServerDeviceName, attributeName)) {
            final DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerDeviceName, false);
            if (proxy != null) {
                try {
                    if (isSynchronizeDeviceAccesses()) {
                        synchronized (proxy) {
                            value = readStringAttributeNoLock(proxy, attributeName);
                        }
                    } else {
                        value = readStringAttributeNoLock(proxy, attributeName);
                    }
                } catch (final DevFailed e) {
                    LOGGER.debug(CANNOT_READ, scanServerDeviceName, attributeName,
                            TangoExceptionHelper.getErrorMessage(e), e);
                }
            }
        }
        return value;
    }

    protected static double readDoubleAttributeNoLock(DeviceProxy proxy, String attributeName) throws DevFailed {
        DeviceAttribute deviceAttribute = proxy.read_attribute(attributeName);
        return deviceAttribute.extractDouble();
    }

    public static double readDoubleAttribute(String scanServerDeviceName, String attributeName) {
        double value = Double.NaN;
        if (TangoAttributeHelper.isAttributeRunning(scanServerDeviceName, attributeName)) {
            final DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerDeviceName, false);
            if (proxy != null) {
                try {
                    if (isSynchronizeDeviceAccesses()) {
                        synchronized (proxy) {
                            value = readDoubleAttributeNoLock(proxy, attributeName);
                        }
                    } else {
                        value = readDoubleAttributeNoLock(proxy, attributeName);
                    }
                } catch (final DevFailed e) {
                    LOGGER.debug(CANNOT_READ, scanServerDeviceName, attributeName,
                            TangoExceptionHelper.getErrorMessage(e), e);
                }
            }
        }
        return value;
    }

    public static boolean isSpectrum(String aScanServerDeviceName, String attributeName) {
        boolean isSpectrum = true;
        int format = TangoAttributeHelper.getAttributeType(aScanServerDeviceName, attributeName);
        if (format == TangoConstHelper.IMAGE_TYPE) {
            isSpectrum = false;
        }
        return isSpectrum;
    }

    protected static void setAttributeInfoNoLock(DeviceProxy proxy, AttributeInfo... attributeInfo) throws DevFailed {
        proxy.set_attribute_info(attributeInfo);
    }

    public static String readAttributeLabel(String scanServerName, String attributeName) {
        String label = null;
        if ((attributeName != null) && (scanServerName != null)) {
            label = scanServerName.toLowerCase() + TangoDeviceHelper.SLASH + attributeName.toLowerCase();
            if (!attributeName.endsWith(TIMESTAMPS)) {
                label = TangoAttributeHelper.getLabel(scanServerName, attributeName);
                if ((label != null) && attributeName.startsWith(DATA)) {
                    label = label + SENSOR;
                }
            }
            // TODO write Display Unit properties because it is not done
            // Set 1000 to convert the value in ms
            else {
                TangoAttributeHelper.isAttributeRunning(scanServerName, attributeName);
                AttributeInfo attributeInfo = TangoAttributeHelper.getAttributeInfo(scanServerName, attributeName);
                if (attributeInfo != null) {
                    if (!THOUSAND.equals(attributeInfo.display_unit)) {
                        attributeInfo.display_unit = THOUSAND;
                        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
                        if (proxy != null) {
                            try {
                                if (isSynchronizeDeviceAccesses()) {
                                    synchronized (proxy) {
                                        setAttributeInfoNoLock(proxy, attributeInfo);
                                    }
                                } else {
                                    setAttributeInfoNoLock(proxy, attributeInfo);
                                }
                            } catch (DevFailed e) {
                                LOGGER.error(CANNOT_WRITE_DISPLAY_UNIT_1000, scanServerName, attributeName,
                                        TangoExceptionHelper.getErrorMessage(e));
                                LOGGER.debug(STACK_TRACE, e);
                            }
                        }
                    }
                }

            }
        }
        return label;
    }

    public static String getAttributeLabel(String scanServerName, String attributeName) {
        String label = null;
        ScanServerDataModel model = getDataModel(scanServerName, attributeName);
        if (model != null) {
            // Bug Mantis 0026823 force the label readind each time
            label = readAttributeLabel(scanServerName, attributeName);
            if (label != null) {
                ATT_NAME.put(label, (scanServerName + TangoDeviceHelper.SLASH + attributeName).toLowerCase());
                model.setAttributeLabel(label);
            }
        }
        return label;
    }

    public static String getAttributeName(String completeAttributeLabel) {
        String attributeName = completeAttributeLabel;
        if ((completeAttributeLabel != null) && ATT_NAME.containsKey(completeAttributeLabel.toLowerCase())) {
            attributeName = ATT_NAME.get(completeAttributeLabel.toLowerCase());
        }
        return attributeName;
    }

    private static Map<String, ScanServerDataModel> getDataModelMap(String scanServerName) {
        Map<String, ScanServerDataModel> newMap = null;
        if ((scanServerName != null) && !scanServerName.isEmpty()) {
            newMap = SCAN_SERVER_DATA_MODEL_MAP.get(scanServerName.toLowerCase());
            if (newMap == null) {
                newMap = new HashMap<>();
                SCAN_SERVER_DATA_MODEL_MAP.put(scanServerName.toLowerCase(), newMap);
            }
        }
        return newMap;
    }

    public static int getDataModelSize(String scanServerName) {
        int size = 0;
        Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
        if (map != null) {
            size = map.size();
        }
        return size;
    }

    public static void setAttributeAxis(String scanServerName, String attributeName, String axis) {
        ScanServerDataModel model = getDataModel(scanServerName, attributeName, false);
        if ((axis != null) && (model != null)) {
            if (axis.equals(Axis.X)) {
                setActuatorName(scanServerName, attributeName);
            } else {
                model.setAxis(axis);
            }
            getCurrentScanProperties(scanServerName, true);
        }
    }

    private static void setAllAttributeAxis(String scanServerName, String axis) {
        Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
        if (map != null) {
            for (ScanServerDataModel dataModel : map.values()) {
                dataModel.setAxis(axis);
            }
        }
    }

    public static void setAttributeListAxis(String scanServerName, String[] attributeNameList, final String axis) {
        if (attributeNameList != null) {
            for (String attributeName : attributeNameList) {
                CurrentScanDataModel.setAttributeAxis(scanServerName, attributeName, axis);
            }
        }
    }

    private static void setAttributeListAxis(String scanServerName, List<String> attributeNameList, final String axis) {
        if (attributeNameList != null) {
            for (String attributeName : attributeNameList) {
                ScanServerDataModel model = getDataModel(scanServerName, attributeName, false);
                if (model != null) {
                    model.setAxis(axis);
                }
            }
        }
    }

    public static void updateDataModel(String scanServerName) {
        if ((scanServerName != null) && !scanServerName.isEmpty()) {
            if (dateChanged(scanServerName)) {
                updateScanServerInfo(scanServerName);
            }
        }
    }

    // Fill the attribute information link to the scan server
    private static void updateScanServerInfo(String scanServerName) {
        updateScanServerInfo(scanServerName, true);
    }

    private static void updateScanServerInfo(String scanServerName, boolean withNexus) {
        // memorize the axis setting
        if (scanServerName != null) {
            clearDataModelMap(scanServerName);

            // XActuatorList
            readActuators(scanServerName);
            String[] stringList = ATT_STRING_SPECTRUM_VALUE
                    .get((scanServerName + TangoDeviceHelper.SLASH + ACTUATORS_DATA_LIST).toLowerCase());
            updateAttributeList(scanServerName, stringList);

            // YActuatorList
            stringList = ATT_STRING_SPECTRUM_VALUE
                    .get((scanServerName + TangoDeviceHelper.SLASH + YACTUATORS_DATA_LIST).toLowerCase());
            updateAttributeList(scanServerName, stringList);

            // Y1 SensorList or sensor 2D
            readSensors(scanServerName);
            stringList = ATT_STRING_SPECTRUM_VALUE
                    .get((scanServerName + TangoDeviceHelper.SLASH + SENSORS_DATA_LIST).toLowerCase());
            updateAttributeList(scanServerName, stringList);

            // SensorTimeStamp
            updateAttribute(scanServerName, SENSORS_TIMESTAMPS);

            // ActuatorTimeStamp
            updateAttribute(scanServerName, ACTUATORS_TIMESTAMPS);

            // Set properties
            ScanProperties newproperties = getCurrentScanProperties(scanServerName, false);
            setAllAttributeAxis(scanServerName, Axis.NONE);
            if (newproperties != null) {
                // System.out.println("newProperties= " + newproperties);
                ScanServerDataModel model = getDataModel(scanServerName, newproperties.getXAttribute(), false);
                if (model != null) {
                    model.setAxis(Axis.X);
                }

                setAttributeListAxis(scanServerName, newproperties.getY1AttributeList(), Axis.Y1);
                setAttributeListAxis(scanServerName, newproperties.getY2AttributeList(), Axis.Y2);
                setAttributeListAxis(scanServerName, newproperties.getZAttributeList(), Axis.Z);
            }

            NEXUS_FILE_NAME_MAP.remove(scanServerName.toLowerCase());
            if (withNexus) {
                updateNexusInfo(scanServerName);
            }
        }
    }

    private static void updateAttributeList(String scanServerName, String[] attributeList) {
        if ((attributeList != null) && (scanServerName != null)) {
            for (String attributeName : attributeList) {
                updateAttribute(scanServerName, attributeName);
            }
        }
    }

    private static void updateAttribute(String scanServerName, String attributeName) {
        if ((attributeName != null) && (scanServerName != null)) {
            String label = readAttributeLabel(scanServerName, attributeName);
            ATT_NAME.put(label, (scanServerName + TangoDeviceHelper.SLASH + attributeName).toLowerCase());
            ScanServerDataModel model = getDataModel(scanServerName, attributeName, true);
            if (model != null) {
                model.setAttributeLabel(label);
                model.setSpectrum(isSpectrum(scanServerName, attributeName));
            }
        }
    }

    private static void readNxEntryNoLock(String scanServerName, DeviceProxy dataRecorderProxy) throws DevFailed {
        DeviceAttribute nxEntryAttribute = dataRecorderProxy.read_attribute(NX_ENTRY);
        if (nxEntryAttribute != null) {
            String nxEntryName = nxEntryAttribute.extractString();
            NEXUS_FILE_ENTRY_MAP.put(scanServerName.toLowerCase(), nxEntryName);
        }
    }

    private static boolean isDataRecordedNoLock(DeviceProxy scanProxy) throws DevFailed {
        DeviceAttribute attribute = scanProxy.read_attribute(DATA_RECORDED);
        return attribute.extractBoolean();
    }

    private static String readNexusFileNameNoLock(String scanServerName, DeviceProxy scanProxy, int tryCount)
            throws DevFailed {
        String nexusFileName = null;
        DeviceAttribute attribute = scanProxy.read_attribute(NEXUS_FILE);
        if (attribute == null) {
            LOGGER.error(CANNOT_READ_ATTRIBUTE_RETURNED_ATTRIBUTE_IS_NULL_TRY_NB, scanServerName, NEXUS_FILE, tryCount);
        } else {
            nexusFileName = attribute.extractString();
            if (nexusFileName == null) {
                nexusFileName = ObjectUtils.EMPTY_STRING;
            }
            if (nexusFileName.isEmpty()) {
                LOGGER.warn(ATTRIBUTE_HAS_AN_EMPTY_VALUE_TRY_NB, scanServerName, NEXUS_FILE, tryCount);
            } else {
                LOGGER.info(VALUE_EQUALS, scanServerName, NEXUS_FILE, nexusFileName);
            }
        }
        return nexusFileName;
    }

    private static void updateNexusInfo(String scanServerName) {
        if (scanServerName != null) {
            NEXUS_FILE_NAME_MAP.remove(scanServerName.toLowerCase());
            NEXUS_FILE_ENTRY_MAP.remove(scanServerName.toLowerCase());
            if (TangoAttributeHelper.isAttributeRunning(scanServerName, NEXUS_FILE)) {
                // Read the property of DataRecorder = storage/recorder/datarecorder.1/
                DeviceProxy scanProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
                if (scanProxy != null) {
                    try {
                        boolean dataRecorded;
                        if (isSynchronizeDeviceAccesses()) {
                            synchronized (scanProxy) {
                                dataRecorded = isDataRecordedNoLock(scanProxy);
                            }
                        } else {
                            dataRecorded = isDataRecordedNoLock(scanProxy);
                        }
                        if (dataRecorded) {
                            String nexusFileName = null;
                            long startSTMP = System.currentTimeMillis();
                            long durationTry = 0;
                            int tryCount = 0;
                            while ((durationTry < 5000) && ((nexusFileName == null) || nexusFileName.isEmpty())) {
                                try {
                                    if (isSynchronizeDeviceAccesses()) {
                                        synchronized (scanProxy) {
                                            nexusFileName = readNexusFileNameNoLock(scanServerName, scanProxy,
                                                    ++tryCount);
                                        }
                                    } else {
                                        nexusFileName = readNexusFileNameNoLock(scanServerName, scanProxy, ++tryCount);
                                    }
                                } catch (Exception e) {
                                    LOGGER.error(CANNOT_READ_ATTRIBUTE_N_TRY_NB, scanServerName, NEXUS_FILE,
                                            TangoExceptionHelper.getErrorMessage(e), tryCount);
                                    LOGGER.debug(STACK_TRACE, e);
                                }
                                durationTry = System.currentTimeMillis() - startSTMP;
                                // Read the attribute not to fast
                                try {
                                    Thread.sleep(200);
                                } catch (InterruptedException e) {
                                }
                            }
                            if (nexusFileName == null) {
                                nexusFileName = ObjectUtils.EMPTY_STRING;
                            }

                            if (nexusFileName.isEmpty()) {
                                if (durationTry >= 5000) {
                                    LOGGER.warn(NO_NEXUS_FILE_GENERATED_IT_TOOK_TOO_MUCH_TIME, scanServerName,
                                            NEXUS_FILE);
                                } else {
                                    LOGGER.warn(NO_NEXUS_FILE_GENERATED);
                                }
                            }

                            NEXUS_FILE_NAME_MAP.put(scanServerName.toLowerCase(), nexusFileName);
                            String dataRecorderDeviceName = DEFAULT_RECORDER;
                            Database database = TangoDeviceHelper.getDatabase();
                            if (database != null) {
                                DbDatum dbDatum = database.get_device_property(scanServerName, DATA_RECORDER);
                                if (dbDatum != null) {
                                    dataRecorderDeviceName = dbDatum.extractString();
                                }

                                if (TangoDeviceHelper.isDeviceRunning(dataRecorderDeviceName)) {
                                    DeviceProxy dataRecorderProxy = TangoDeviceHelper
                                            .getDeviceProxy(dataRecorderDeviceName);
                                    if (dataRecorderProxy != null) {
                                        if (isSynchronizeDeviceAccesses()) {
                                            synchronized (dataRecorderProxy) {
                                                readNxEntryNoLock(scanServerName, dataRecorderProxy);
                                            }
                                        } else {
                                            readNxEntryNoLock(scanServerName, dataRecorderProxy);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (DevFailed e) {
                        LOGGER.error(CANNOT_UPDATE_NEXUS_INFO, TangoExceptionHelper.getErrorMessage(e));
                        LOGGER.debug(STACK_TRACE, e);
                    }
                }
            }
        }
    }

    private static List<String> buildAttributeAxisList(String scanServerName, String axis) {
        List<String> attributeList = null;
        Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
        if (map != null) {
            attributeList = new ArrayList<>();
            for (ScanServerDataModel tmpDataModel : map.values()) {
                if (tmpDataModel.getAxis().equals(axis)) {
                    attributeList.add(tmpDataModel.getAttributeName());
                }
            }
        }
        return attributeList;
    }

    public static String[] buildAttributeList(String scanServerName, String axis) {
        String[] result = null;
        List<String> attributeList = buildAttributeAxisList(scanServerName, axis);
        if (attributeList != null) {
            result = trimArrayAndSort(attributeList.toArray(new String[attributeList.size()]));
        }
        return result;
    }

    // Remove null, empty string and sort the array
    public static String[] trimArrayAndSort(String[] array) {
        if (array == null) {
            return new String[0];
        }

        List<String> strVector = new ArrayList<>();
        String tmpString = null;
        for (String element : array) {
            tmpString = element;
            if ((tmpString != null) && !tmpString.isEmpty() && !strVector.contains(tmpString)) {
                strVector.add(tmpString);
            }
        }
        String[] newArray = strVector.toArray(new String[strVector.size()]);
        Arrays.sort(newArray);
        strVector.clear();
        strVector = null;
        return newArray;
    }

    public static String getNexusFileName(String scanServerName) {
        String fileName = null;
        if (scanServerName != null) {
            fileName = NEXUS_FILE_NAME_MAP.get(scanServerName.toLowerCase());
        }
        if (fileName == null) {
            updateNexusInfo(scanServerName);
            fileName = NEXUS_FILE_NAME_MAP.get(scanServerName.toLowerCase());
        }
        return fileName;
    }

    public static String getNexusFileEntry(String scanServerName) {
        String nexusEntry = null;
        if (scanServerName != null) {
            nexusEntry = NEXUS_FILE_ENTRY_MAP.get(scanServerName.toLowerCase());
        }
        if (nexusEntry == null) {
            updateNexusInfo(scanServerName);
            nexusEntry = NEXUS_FILE_ENTRY_MAP.get(scanServerName.toLowerCase());
        }
        return nexusEntry;
    }

    public static String getActuatorName(String scanServerName) {
        String actuatorName = ObjectUtils.EMPTY_STRING;
        ScanServerDataModel dataModel = getXAttributeModel(scanServerName);
        if (dataModel != null) {
            actuatorName = dataModel.getAttributeName();
        }
        return actuatorName;
    }

    private static ScanServerDataModel getXAttributeModel(String scanServerName) {
        ScanServerDataModel dataModel = null;
        Map<String, ScanServerDataModel> map = getDataModelMap(scanServerName);
        if (map != null) {
            for (ScanServerDataModel tmpDataModel : map.values()) {
                if ((tmpDataModel != null) && tmpDataModel.getAxis().equals(Axis.X)) {
                    dataModel = tmpDataModel;
                    break;
                }
            }
        }
        return dataModel;
    }

    public static void setActuatorName(String scanServerName, String newActuatorName) {
        ScanServerDataModel oldXDataModel = getXAttributeModel(scanServerName);
        ScanServerDataModel newXDataModel = CurrentScanDataModel.getDataModel(scanServerName, newActuatorName, false);
        if ((newXDataModel != null) && !newXDataModel.equals(oldXDataModel)) {
            if (oldXDataModel != null) {
                oldXDataModel.setAxis(Axis.NONE);
            }
            newXDataModel.setAxis(Axis.X);
        }
    }

    protected static String[] readAttributeListNoLock(String scanServerDeviceName, DeviceProxy proxy,
            String actuatorsName) throws DevFailed {
        DeviceAttribute attribute = proxy.read_attribute(actuatorsName);
        String[] allattributeList = attribute.extractStringArray();
        String[] attributeList = Arrays.copyOf(allattributeList, attribute.getNbRead());
        LOGGER.trace(VALUE_EQUALS, scanServerDeviceName, actuatorsName, Arrays.toString(attributeList));
        return attributeList;
    }

    public static String[] readAttributeList(String scanServerDeviceName, String actuatorsName) {
        String[] attributeList = null;
        if (TangoAttributeHelper.isAttributeRunning(scanServerDeviceName, actuatorsName)) {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(scanServerDeviceName, false);
            if (proxy != null) {
                try {
                    if (isSynchronizeDeviceAccesses()) {
                        synchronized (proxy) {
                            attributeList = readAttributeListNoLock(scanServerDeviceName, proxy, actuatorsName);
                        }
                    } else {
                        attributeList = readAttributeListNoLock(scanServerDeviceName, proxy, actuatorsName);
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerDeviceName, actuatorsName,
                            TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            }
        }
        return attributeList;
    }

    public static String[] getActuators(String scanServerName) {
        String key = (scanServerName + TangoDeviceHelper.SLASH + ACTUATORS_DATA_LIST + ALL).toLowerCase();
        String[] actuatorList = ATT_STRING_SPECTRUM_VALUE.get(key);
        if (actuatorList == null) {
            actuatorList = readActuators(scanServerName);
        }
        return actuatorList;
    }

    private static String[] readActuators(String scanServerName) {
        String[] actuatorsList = null;
        // YActuatorList
        String[] xActuatorsList = readAttributeList(scanServerName, ACTUATORS_DATA_LIST);
        if (xActuatorsList != null) {
            ATT_STRING_SPECTRUM_VALUE.put(
                    (scanServerName + TangoDeviceHelper.SLASH + ACTUATORS_DATA_LIST).toLowerCase(), xActuatorsList);
            xActuatorsList = trimArrayAndSort(xActuatorsList);
        }

        // YActuatorList
        String[] yActuatorsList = readAttributeList(scanServerName, YACTUATORS_DATA_LIST);
        if (yActuatorsList != null) {
            yActuatorsList = readAttributeList(scanServerName, YACTUATORS_DATA_LIST);
            ATT_STRING_SPECTRUM_VALUE.put(
                    (scanServerName + TangoDeviceHelper.SLASH + YACTUATORS_DATA_LIST).toLowerCase(), yActuatorsList);
            yActuatorsList = trimArrayAndSort(yActuatorsList);
        }

        if ((xActuatorsList != null) && (yActuatorsList != null)) {
            actuatorsList = new String[xActuatorsList.length + yActuatorsList.length];
            int index = 0;
            for (String actuatorName : xActuatorsList) {
                actuatorsList[index] = scanServerName + TangoDeviceHelper.SLASH + actuatorName;
                index++;
            }
            for (String actuatorName : yActuatorsList) {
                actuatorsList[index] = scanServerName + TangoDeviceHelper.SLASH + actuatorName;
                index++;
            }
        }

        if (actuatorsList != null) {
            ATT_STRING_SPECTRUM_VALUE.put(
                    (scanServerName + TangoDeviceHelper.SLASH + ACTUATORS_DATA_LIST + ALL).toLowerCase(),
                    actuatorsList);
        }

        return actuatorsList;
    }

    public static String[] getSensors(String scanServerName) {
        String[] sensorList = null;
        String key = (scanServerName + TangoDeviceHelper.SLASH + SENSORS_DATA_LIST).toLowerCase();
        String[] simpleSensorsList = ATT_STRING_SPECTRUM_VALUE.get(key);
        if (simpleSensorsList == null) {
            simpleSensorsList = readSensors(scanServerName);
        }

        if (simpleSensorsList != null) {
            sensorList = new String[simpleSensorsList.length];
            for (int i = 0; i < simpleSensorsList.length; i++) {
                sensorList[i] = scanServerName + TangoDeviceHelper.SLASH + simpleSensorsList[i];

            }
        }
        return sensorList;
    }

    private static String[] readSensors(String scanServerName) {
        String[] simpleSensorsList = readAttributeList(scanServerName, SENSORS_DATA_LIST);
        if (simpleSensorsList != null) {
            simpleSensorsList = trimArrayAndSort(simpleSensorsList);
            ATT_STRING_SPECTRUM_VALUE.put((scanServerName + TangoDeviceHelper.SLASH + SENSORS_DATA_LIST).toLowerCase(),
                    simpleSensorsList);
        }
        return simpleSensorsList;
    }

    public static String[] readHistoric(String scanServerName) {
        String[] historicList = readAttributeList(scanServerName, HISTORIC);
        return historicList;
    }

    protected static int getIntegrationTimeNoLock(DeviceProxy proxy) throws DevFailed {
        int integrationTime;
        DeviceAttribute deviceAttribute = proxy.read_attribute(INTEGRATION_TIMES);
        double[] values = deviceAttribute.extractDoubleArray();
        if ((values != null) && (values.length > 0)) {
            integrationTime = (int) (values[0] * 1000);
        } else {
            integrationTime = TangoDataSourceFactory.DEFAULT_SLEEPING_PERIOD;
        }
        return integrationTime;
    }

    public static int getIntegrationTime(String scanServerName) {
        int integrationTime = TangoDataSourceFactory.DEFAULT_SLEEPING_PERIOD;
        if (TangoAttributeHelper.isAttributeRunning(scanServerName, INTEGRATION_TIMES)) {
            DeviceProxy deviceProxy = TangoDeviceHelper.getDeviceProxy(scanServerName, false);
            if (deviceProxy != null) {
                try {
                    if (isSynchronizeDeviceAccesses()) {
                        synchronized (deviceProxy) {
                            integrationTime = getIntegrationTimeNoLock(deviceProxy);
                        }
                    } else {
                        integrationTime = getIntegrationTimeNoLock(deviceProxy);
                    }
                } catch (DevFailed e) {
                    LOGGER.error(CANNOT_READ, scanServerName, INTEGRATION_TIMES,
                            TangoExceptionHelper.getErrorMessage(e));
                    LOGGER.debug(STACK_TRACE, e);
                }
            }
        }
        return integrationTime;
    }
}
