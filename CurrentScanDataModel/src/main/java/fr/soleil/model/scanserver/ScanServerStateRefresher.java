package fr.soleil.model.scanserver;

public class ScanServerStateRefresher extends Thread {

    private String scanServerName = null;

    public ScanServerStateRefresher(final String scanServerName) {
        // Name this thread (detected in SCAN-896).
        super(ScanServerStateRefresher.class.getSimpleName() + "(" + scanServerName + ")");
        this.scanServerName = scanServerName;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            CurrentScanDataModel.fireStateChanged(scanServerName, CurrentScanDataModel.getState(scanServerName));
        }
    }
}
