package fr.soleil.model.scanserver;

public interface IScanServerListener {

    public void stateChanged(String state);

}
