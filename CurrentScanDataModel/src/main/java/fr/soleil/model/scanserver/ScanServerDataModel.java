package fr.soleil.model.scanserver;

import fr.soleil.lib.project.ObjectUtils;

public class ScanServerDataModel {

    private String attributeName = null;
    private String attributeLabel = null;
    private final Axis axis = new Axis();

    protected ScanServerDataModel(final String anAttributeName) {
        this.attributeName = anAttributeName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String anAttributeName) {
        this.attributeName = anAttributeName;
    }

    public String getAttributeLabel() {
        return attributeLabel;
    }

    public void setAttributeLabel(String anAttributeLabel) {
        this.attributeLabel = anAttributeLabel;
    }

    public Axis getAxisValue() {
        return this.axis;
    }

    public String getAxis() {
        return this.axis.getAxis();
    }

    public void setAxis(String anAxis) {
        this.axis.setAxis(anAxis);
    }

    public void setSpectrum(boolean spectrum) {
        this.axis.setSpectrum(spectrum);
    }

    public boolean isSpectrum() {
        return this.axis.isSpectrum();
    }

    @Override
    public String toString() {
        return attributeLabel;
    }

    public ScanServerDataModel getCopy() {
        ScanServerDataModel model = new ScanServerDataModel(attributeName);
        model.setAttributeLabel(attributeLabel);
        model.setAxis(getAxis());
        model.setSpectrum(isSpectrum());
        return model;

    }

    @Override
    public boolean equals(Object obj) {
        boolean equals = false;
        if (obj instanceof ScanServerDataModel) {
            ScanServerDataModel compare = (ScanServerDataModel) obj;
            if (ObjectUtils.sameObject(attributeName, compare.getAttributeName())
                    && ObjectUtils.sameObject(attributeLabel, compare.getAttributeLabel())
                    && ObjectUtils.sameObject(getAxis(), compare.getAxis()) && isSpectrum() == compare.isSpectrum()) {
                equals = true;
            }
        }
        return equals;
    }
}
