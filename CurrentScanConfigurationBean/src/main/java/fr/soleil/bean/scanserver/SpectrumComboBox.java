package fr.soleil.bean.scanserver;

import javax.swing.JComboBox;

import fr.soleil.model.scanserver.Axis;

public class SpectrumComboBox extends JComboBox<String> {

    private static final long serialVersionUID = 2309870736577649058L;

    public SpectrumComboBox() {
        this(false);
    }

    public SpectrumComboBox(boolean withoutX) {
        addItem(Axis.NONE);
        if (!withoutX) {
            addItem(Axis.X);
        }
        addItem(Axis.Y1);
        addItem(Axis.Y2);
    }
}
