package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.bean.datafitter.Proxy;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.border.CometeTitledBorder;
import fr.soleil.comete.swing.border.util.ColorTargetMode;
import fr.soleil.comete.swing.border.util.TextTargetMode;
import fr.soleil.comete.tango.data.adapter.StateAdapter;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.model.scanserver.Axis;

public class DataFitterConfigurationBean extends JPanel {

    private static final long serialVersionUID = 8172566310709192694L;

    public static final Proxy PROXY_PANEL = new Proxy();
    private static final String FITTED_DATAY = "fitteddatay";
    private static final JPanel AXIS_SELECTION_PANEL = new JPanel();
    protected static final StringScalarBox BOX = new StringScalarBox();
    public static final String DEFAULT_TITLE = "DataFitter";

    static {
        Border border = PROXY_PANEL.getBorder();
        if (border instanceof CometeTitledBorder) {
            CometeTitledBorder ctBorder = (CometeTitledBorder) border;
            BOX.setErrorText(ctBorder, "UNKNOWN");
            BOX.setErrorColor(ctBorder, StateAdapter.UNKNOWN);
            ctBorder.setTitle(DEFAULT_TITLE);
            ctBorder.setTitleColor(null);
            ctBorder.setTextTargetMode(TextTargetMode.TEXT_AS_LABEL_TOOLTIP);
            ctBorder.setColorTargetMode(ColorTargetMode.COMETE_BACKGROUND_AS_LABEL_BACKGROUND);
        }
    }

    private final JComboBox<String> comboBoxSpectrum;

    public DataFitterConfigurationBean() {
        super();
        comboBoxSpectrum = new SpectrumComboBox(true);
        initializeGUI();
        setBackground(Color.CYAN);
    }

    /**
     * Initialization.
     */
    private void initializeGUI() {
        setLayout(new BorderLayout());
        comboBoxSpectrum.setSelectedItem(Axis.NONE);
        add(PROXY_PANEL, BorderLayout.CENTER);
        AXIS_SELECTION_PANEL.add(new JLabel("Axis selection : "));
        AXIS_SELECTION_PANEL.add(comboBoxSpectrum);
        add(AXIS_SELECTION_PANEL, BorderLayout.SOUTH);
    }

    public void setModel(String dataFitterDeviceName) {
        PROXY_PANEL.setModel(dataFitterDeviceName);
        Border border = PROXY_PANEL.getBorder();
        if (dataFitterDeviceName == null) {
            if (border instanceof CometeTitledBorder) {
                CometeTitledBorder ctBorder = (CometeTitledBorder) border;
                BOX.disconnectWidgetFromAll(ctBorder);
                ctBorder.setTitle(DEFAULT_TITLE);
            } else if (border instanceof TitledBorder) {
                ((TitledBorder) border).setTitle(DEFAULT_TITLE);
            }
        } else {
            if (border instanceof TitledBorder) {
                ((TitledBorder) border).setTitle(dataFitterDeviceName);
            } else if (border instanceof CometeTitledBorder) {
                CometeTitledBorder ctBorder = (CometeTitledBorder) border;
                ctBorder.setTitle(dataFitterDeviceName);
                BOX.disconnectWidgetFromAll(ctBorder);
                TangoKey key = new TangoKey();
                TangoKeyTool.registerAttribute(key, dataFitterDeviceName, TangoAttributeHelper.STATUS);
                BOX.connectWidget(ctBorder, key);
            }
        }
    }

    public void setActuatorsList(String[] values) {
        if (values != null && values.length > 0) {
            PROXY_PANEL.setXProxiesChoice(values);
        }
    }

    public void setSensorsList(String[] values) {
        if (values != null && values.length > 0) {
            PROXY_PANEL.setYProxiesChoice(values);
        }
    }

    public void start() {
        PROXY_PANEL.start();
    }

    public void stop() {
        PROXY_PANEL.stop();
    }

    public String getModel() {
        return PROXY_PANEL.getModel();
    }

    public String getFittedAttribute() {
        String fittedAttribute = null;
        String dataFitterModel = PROXY_PANEL.getModel();
        if (dataFitterModel != null && !dataFitterModel.isEmpty()
                && TangoAttributeHelper.isAttributeRunning(dataFitterModel, FITTED_DATAY)) {
            fittedAttribute = dataFitterModel + "/" + FITTED_DATAY;
        }
        return fittedAttribute;
    }

    public void addDataFitterBeanListener(ActionListener listener) {
        if (listener != null) {
            comboBoxSpectrum.addActionListener(listener);
        }
    }

}
