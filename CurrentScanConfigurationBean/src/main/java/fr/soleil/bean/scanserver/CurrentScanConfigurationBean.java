/*******************************************************************************
 * Copyright (c) 2006 Synchrotron SOLEIL
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.All rights reserved. This program and the accompanying materials
 *
 * Contributors: katy.saintin@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/

/**
 * @author saintin
 */

package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.definition.widget.util.CometeFont;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.Axis;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;
import fr.soleil.model.scanserver.ScanServerDataModel;

public class CurrentScanConfigurationBean extends AbstractTangoBox implements IScanServerListener, ActionListener {

    private static final long serialVersionUID = -5904979415538147997L;

    public static final Logger LOGGER = LoggerFactory.getLogger(CurrentScanConfigurationBean.class);
    protected static final String[] COLUMN_NAME = new String[] { "Name", "Label", "Axis" };

    // GUI elements
    /**
     * Name of current scan
     */
    private Label runName;
    /**
     * The table
     */
    private final JTable table;

    private final JScrollPane scrollPanel;

    private boolean dataFitterBeanVisible;
    private String dataFitterDeviceName;

    private final DataFitterConfigurationBean dataFitterBean;

    private final int nbRetry;

    /**
     * Table Model List.
     */
    private ScanTableModel tableModel;

    private String oldDate;
    private String scanServerName;

    private final List<ICurrentScanConfigurationBeanListener> listeners = new ArrayList<ICurrentScanConfigurationBeanListener>();

    public CurrentScanConfigurationBean() {
        this(true);
    }

    public CurrentScanConfigurationBean(boolean allowDisplayImageAsSpectrum) {
        super();
        scanServerName = ObjectUtils.EMPTY_STRING;
        table = new JTable();
        scrollPanel = new JScrollPane(table);
        nbRetry = 5;
        dataFitterBeanVisible = true;
        dataFitterBean = new DataFitterConfigurationBean();
        dataFitterDeviceName = dataFitterBean.getModel();
        initializeGUI(allowDisplayImageAsSpectrum);
    }

    /**
     * Initialization.
     */
    private void initializeGUI(boolean allowDisplayImageAsSpectrum) {
        setLayout(new BorderLayout());
        runName = generateLabel();
        runName.setText("No current scan");
        runName.setHorizontalAlignment(SwingConstants.CENTER);
        runName.setCometeFont(new CometeFont("DIALOG", Font.BOLD | Font.ITALIC, 12));
        add(runName, BorderLayout.NORTH);

        JPanel centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);

        centerPanel.setLayout(new BorderLayout());
        centerPanel.add(scrollPanel, BorderLayout.CENTER);

        scrollPanel.setPreferredSize(new Dimension(300, 200));
        scrollPanel.setAutoscrolls(true);
        scrollPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tableModel = new ScanTableModel();
        table.setModel(tableModel);
        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        int count = table.getColumnModel().getColumnCount();
        if (count > 1) {
            CellToolTypeRenderer renderer = new CellToolTypeRenderer();
            table.getColumnModel().getColumn(0).setCellRenderer(renderer);
            table.getColumnModel().getColumn(1).setCellRenderer(renderer);
        }

        if (count > 2) {
            TableColumn axisColumn = table.getColumnModel().getColumn(2);
            axisColumn.setCellEditor(new AxisCellEditor(allowDisplayImageAsSpectrum));
        }

        dataFitterBean.addDataFitterBeanListener(this);
        add(dataFitterBean, BorderLayout.SOUTH);
        dataFitterBean.setVisible(false);

    }

    public boolean isDataFitterBeanVisible() {
        return dataFitterBeanVisible;
    }

    public void setDataFitterBeanVisible(boolean dataFitterBeanVisible) {
        this.dataFitterBeanVisible = dataFitterBeanVisible;
        setDataFitterDeviceName(getDataFitterDeviceName());
    }

    private void updateTable() {
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            tableModel.fireTableDataChanged();
            table.repaint();
            table.validate();
        });
    }

    public void setScanServerName(String name) {
        scanServerName = name;
    }

    public void setScanServerDeviceName(String name) {
        setScanServerName(name);
        if ((model != null) && !model.equalsIgnoreCase(name)) {
            CurrentScanDataModel.removeScanServerListener(model, this);
            clearGUI();
        }
        if ((name != null) && !name.equalsIgnoreCase(model)) {
            CurrentScanDataModel.addScanServerListener(name, this);
            setModel(name);
            start();
        }
    }

    public void setDataFitterDeviceName(String dataFitterDeviceName) {
        this.dataFitterDeviceName = dataFitterDeviceName;
        if (dataFitterBeanVisible && (dataFitterDeviceName != null) && !dataFitterDeviceName.isEmpty()) {
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                dataFitterBean.setVisible(true);
            });
            dataFitterBean.setModel(dataFitterDeviceName);
            dataFitterBean.start();
        } else {
            dataFitterBean.setModel(null);
            EDTManager.INSTANCE.runInDrawingThread(() -> {
                dataFitterBean.setVisible(false);
            });
            dataFitterBean.stop();
        }
    }

    public String getDataFitterDeviceName() {
        return dataFitterDeviceName;
    }

    public String getFittedAttributeName() {
        return dataFitterBean.getFittedAttribute();
    }

    private void updateAxisList(final String[] attributeNameList, final String axis) {
        CurrentScanDataModel.setAttributeListAxis(getModel(), attributeNameList, axis);
    }

    public String[] getZSensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(scanServerName, Axis.Z);
    }

    public void setZSensorsDataList(String[] sensorsDataList) {
        String[] oldSensorList = getZSensorsDataList();
        String[] newSensorList = CurrentScanDataModel.trimArrayAndSort(sensorsDataList);
        if (!Arrays.equals(oldSensorList, newSensorList)) {
            updateAxisList(newSensorList, Axis.Z);
        }
    }

    public String[] getY1SensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(scanServerName, Axis.Y1);
    }

    public void setY1SensorsDataList(String[] sensorsDataList) {
        String[] oldSensorList = getY1SensorsDataList();
        String[] newSensorList = CurrentScanDataModel.trimArrayAndSort(sensorsDataList);
        if (!Arrays.equals(oldSensorList, newSensorList)) {
            updateAxisList(newSensorList, Axis.Y1);
        }
    }

    public String[] getY2SensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(scanServerName, Axis.Y2);
    }

    public void setY2SensorsDataList(String[] sensorsDataList) {
        String[] oldSensorList = getY2SensorsDataList();
        String[] newSensorList = CurrentScanDataModel.trimArrayAndSort(sensorsDataList);
        if (!Arrays.equals(oldSensorList, newSensorList)) {
            updateAxisList(newSensorList, Axis.Y2);
        }
    }

    public String[] getNoneSensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(scanServerName, Axis.NONE);
    }

    public void setNoneSensorsDataList(final String[] sensorsDataList) {
        String[] oldSensorList = getNoneSensorsDataList();
        String[] newSensorList = CurrentScanDataModel.trimArrayAndSort(sensorsDataList);
        if (!Arrays.equals(oldSensorList, newSensorList)) {
            updateAxisList(newSensorList, Axis.NONE);
        }
    }

    public String getActuatorName() {
        return CurrentScanDataModel.getActuatorName(scanServerName);
    }

    public void setActuatorName(String newActuatorName) {
        CurrentScanDataModel.setActuatorName(scanServerName, newActuatorName);
    }

    private void updateDataModelAxis(final String attributeName, final String newAxis) {
        if (newAxis != null) {
            String scanServerName = getModel();
            ScanServerDataModel dataModel = CurrentScanDataModel.getDataModel(scanServerName, attributeName);
            if (dataModel != null) {
                String oldAxis = dataModel.getAxis();
                CurrentScanDataModel.setAttributeAxis(scanServerName, attributeName, newAxis);
                if ((oldAxis == null) || !newAxis.equals(oldAxis)) {
                    if (oldAxis.equals(Axis.X) || newAxis.equals(Axis.X)) {
                        fireActuatorChanged();
                    }
                    if (oldAxis.equals(Axis.Y1) || newAxis.equals(Axis.Y1)) {
                        fireSensorsY1Changed();
                    }
                    if (oldAxis.equals(Axis.Y2) || newAxis.equals(Axis.Y2)) {
                        fireSensorsY2Changed();
                    }
                    if (oldAxis.equals(Axis.Z) || newAxis.equals(Axis.Z)) {
                        fireSensorsZChanged();
                    }
                    if (oldAxis.equals(Axis.NONE) || newAxis.equals(Axis.NONE)) {
                        fireSensorsNoneChanged();
                        updateTable();
                    }
                }

            }
        }
    }

    public void addCurrentScanConfigurationBeanListener(final ICurrentScanConfigurationBeanListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
            // Refresh dataFitter if the attribute is not running
            String fittedAttribute = dataFitterBean.getFittedAttribute();
            if ((fittedAttribute != null) && (!fittedAttribute.isEmpty())) {
                firefittedNoneDataChanged(fittedAttribute);
            }
        }
    }

    public void removeCurrentScanConfigurationBeanListener(final ICurrentScanConfigurationBeanListener listener) {
        if (listeners.contains(listener)) {
            listeners.remove(listener);
        }
    }

    public void fireSensorsZChanged() {
        for (final ICurrentScanConfigurationBeanListener listener : listeners) {
            listener.sensorsZChanged(getZSensorsDataList());
        }
    }

    public void fireActuatorChanged() {
        for (final ICurrentScanConfigurationBeanListener listener : listeners) {
            listener.actuatorChanged(getActuatorName());
        }
    }

    public void fireSensorsY1Changed() {
        (new Thread("CurrentScanConfigurationBean.fireSensorsY1Changed") {
            @Override
            public void run() {
                for (final ICurrentScanConfigurationBeanListener listener : listeners) {
                    listener.sensorsY1Changed(getY1SensorsDataList());
                }
            }
        }).start();

    }

    public void fireSensorsY2Changed() {
        (new Thread("CurrentScanConfigurationBean.fireSensorsY2Changed") {
            @Override
            public void run() {
                for (final ICurrentScanConfigurationBeanListener listener : listeners) {
                    listener.sensorsY2Changed(getY2SensorsDataList());
                }
            }
        }).start();

    }

    public void fireSensorsNoneChanged() {
        (new Thread("CurrentScanConfigurationBean.fireSensorsNoneChanged") {
            @Override
            public void run() {
                for (final ICurrentScanConfigurationBeanListener listener : listeners) {
                    listener.sensorsNoneChanged(getNoneSensorsDataList());
                }
            }
        }).start();

    }

    public void firefittedY1DataChanged(String attributeName) {
        for (final ICurrentScanConfigurationBeanListener listener : listeners) {
            listener.fittedY1DataChanged(attributeName);
        }
    }

    public void firefittedY2DataChanged(String attributeName) {
        for (final ICurrentScanConfigurationBeanListener listener : listeners) {
            listener.fittedY2DataChanged(attributeName);
        }
    }

    public void firefittedNoneDataChanged(String attributeName) {
        for (final ICurrentScanConfigurationBeanListener listener : listeners) {
            listener.fittedNoneDataChanged(attributeName);
        }
    }

    @Override
    protected void refreshGUI() {
        TangoKey runNameKey = generateAttributeKey(CurrentScanDataModel.RUN_NAME);
        stringBox.connectWidget(runName, runNameKey);
        // connectAttributeMonitoredDevices();
        // connectMonitoredDeviceStateAttribute();
    }

    @Override
    protected void clearGUI() {
        cleanWidget(runName);
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void savePreferences(Preferences preferences) {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
    }

    @Override
    public void stateChanged(String state) {
        String scanServerName = getModel();
        if (CurrentScanDataModel.dateChanged(scanServerName, oldDate)) {
            oldDate = CurrentScanDataModel.getStartDate(scanServerName);
            LOGGER.info(CurrentScanDataModel.RUN_START_DATE + " has changed " + oldDate);
            // BUG JIRA SCAN-146 read all the new attributes while there is no error
            boolean error = true;
            int nbTry = 0;
            while ((nbTry < nbRetry) && error) {
                error = readScanInformations();
                nbTry++;
                if (error) {
                    LOGGER.error("read scan informations failed try :{}", nbTry);
                }
                try {
                    // Try to
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    LOGGER.debug(e.getMessage());
                    LOGGER.debug("Stack trace", e);
                }
            }
        }
        // Refresh dataFitter if the attribute is not running
        String fittedAttribute = dataFitterBean.getFittedAttribute();
        if ((fittedAttribute == null) || (fittedAttribute.isEmpty())) {
            firefittedNoneDataChanged(fittedAttribute);
        }
    }

    private boolean readScanInformations() {
        boolean error = false;

        // Read all information
        CurrentScanDataModel.updateDataModel(scanServerName);

        List<String> actuatorsList = new ArrayList<String>();

        // Check the sensor
        String[] sensorsList = CurrentScanDataModel.getSensors(scanServerName);
        if ((sensorsList != null) && (sensorsList.length != 0)) {
            LOGGER.info("Check sensors creation {}", Arrays.toString(sensorsList));
            String deviceName = null;
            String attributeName = null;
            for (String sensorName : sensorsList) {
                actuatorsList.add(sensorName);
                deviceName = TangoDeviceHelper.getDeviceName(sensorName);
                attributeName = TangoDeviceHelper.getEntityName(sensorName);
                if (!TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                    LOGGER.error("{} is not created yet !", sensorName);
                    error = true;
                    break;
                }
            }
        } else {
            error = true;
        }

        if (!error) {
            String[] actuatorList = CurrentScanDataModel.getActuators(scanServerName);
            if ((actuatorList != null) && (actuatorList.length != 0)) {
                LOGGER.info("Check actuators creation {}", Arrays.toString(actuatorList));
                String deviceName = null;
                String attributeName = null;
                for (String actuatorName : actuatorList) {
                    actuatorsList.add(actuatorName);
                    deviceName = TangoDeviceHelper.getDeviceName(actuatorName);
                    attributeName = TangoDeviceHelper.getEntityName(actuatorName);
                    if (!TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                        LOGGER.error("{} is not created yet !", actuatorName);
                        error = true;
                        break;
                    }
                }
            }
        }

        if (!error) {
            String[] actuatorArray = actuatorsList.toArray(new String[actuatorsList.size()]);
            LOGGER.info("All dynamic attributes are correctly created {}", Arrays.toString(actuatorArray));
            dataFitterBean.setActuatorsList(actuatorArray);
            dataFitterBean.setSensorsList(sensorsList);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    updateTable();
                }
            });
        }

        return error;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        String dataFitterDeviceName = getDataFitterDeviceName();
        String fittedAttribute = dataFitterBean.getFittedAttribute();
        if ((fittedAttribute != null) && (!fittedAttribute.isEmpty()) && (dataFitterDeviceName != null)
                && (!dataFitterDeviceName.isEmpty())) {
            String actionCommand = arg0.getActionCommand();
            if ((arg0.getModifiers() == InputEvent.BUTTON1_MASK) || !actionCommand.equals("comboBoxChanged")) {
                Object source = arg0.getSource();
                if ((source != null) && (source instanceof SpectrumComboBox)) {
                    SpectrumComboBox combo = (SpectrumComboBox) source;
                    String sItem = (String) combo.getSelectedItem();
                    if (sItem != null) {
                        if (sItem.equals(Axis.Y1)) {
                            firefittedY1DataChanged(fittedAttribute);
                        } else if (sItem.equals(Axis.Y2)) {
                            firefittedY2DataChanged(fittedAttribute);
                        } else {
                            firefittedNoneDataChanged(fittedAttribute);
                        }
                    }
                }
            }
        } else {
            firefittedNoneDataChanged(fittedAttribute);
        }
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        String scanServer = "test/scan/julien";
        String dataFitter = "ICA/SALSA/FIT.1";

        if ((args != null) && (args.length > 0)) {
            scanServer = args[0];
        }

        CurrentScanConfigurationBean bean = new CurrentScanConfigurationBean();
        bean.setScanServerDeviceName(scanServer);
        bean.setDataFitterDeviceName(dataFitter);
        bean.start();

        JFrame frame = new JFrame();
        frame.setContentPane(bean);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    public class ScanTableModel extends DefaultTableModel {

        private static final long serialVersionUID = 2361612981702779147L;

        @Override
        public boolean isCellEditable(final int row, final int column) {
            return column == 2 ? true : false;
        }

        @Override
        public Object getValueAt(final int row, final int column) {
            Object value;
            ScanServerDataModel dataModel = CurrentScanDataModel.getDataModel(getModel(), row);
            if (dataModel == null) {
                value = ObjectUtils.EMPTY_STRING;
            } else {
                switch (column) {
                    case 0:
                        value = dataModel.getAttributeName();
                        break;
                    case 1:
                        value = dataModel.getAttributeLabel();
                        break;
                    case 2:
                        value = dataModel.getAxisValue();
                        break;
                    default:
                        value = ObjectUtils.EMPTY_STRING;
                        break;
                }
            }
            return value;
        }

        @Override
        public void setValueAt(final Object value, final int row, final int column) {
            if (value instanceof Axis) {
                final Object attributeName = getValueAt(row, 0);
                final Axis axis = (Axis) value;
                if (attributeName != null) {
                    (new Thread("ScanTableModel.updateDataModelAxis") {
                        @Override
                        public void run() {
                            updateDataModelAxis(attributeName.toString(), axis.getAxis());
                        };
                    }).start();
                }
            }
        }

        @Override
        public Class<?> getColumnClass(int arg0) {
            if (arg0 == 2) {
                return Axis.class;
            }

            return String.class;
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public String getColumnName(int arg0) {
            return COLUMN_NAME[arg0];
        }

        @Override
        public int getRowCount() {
            int rowCount = CurrentScanDataModel.getDataModelSize(getModel());
            return rowCount;
        }

    }

}
