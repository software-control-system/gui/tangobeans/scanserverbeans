//+============================================================================
//Source: package fr.soleil.comete.widget.editor;/ObjectEditor.java
//
//project :     Comete
//
//Description: This class hides
//
//Author: SAINTIN
//
//Revision: 1.1
//
//Log:
//
//copyleft :Synchrotron SOLEIL
//			L'Orme des Merisiers
//			Saint-Aubin - BP 48
//			91192 GIF-sur-YVETTE CEDEX
//			FRANCE
//
//+============================================================================
package fr.soleil.bean.scanserver;

import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JTable;

import fr.soleil.model.scanserver.Axis;

public class AxisCellEditor extends DefaultCellEditor {

    private static final long serialVersionUID = -6613973648755544493L;

    private static final String[] SPECTRUM_CHOICES = { Axis.NONE, Axis.Y1, Axis.Y2, Axis.X };
//    private static String[] imageChoices;

    private DefaultComboBoxModel<String> spectrumComboBoxModel;
    private DefaultComboBoxModel<String> imageComboBoxModel;
    private Axis currentValue = null;

    public AxisCellEditor(boolean allowDisplayImageAsSpectrum) {
        super(new JComboBox<>());
        String[] defaultImageChoices = { Axis.NONE, Axis.Z };
        String[] fullImageChoices = { Axis.NONE, Axis.Z, Axis.Y1, Axis.Y2, Axis.X };

        if (allowDisplayImageAsSpectrum) {
            imageComboBoxModel = new DefaultComboBoxModel<>(fullImageChoices);
        } else {
            imageComboBoxModel = new DefaultComboBoxModel<>(defaultImageChoices);
        }
        spectrumComboBoxModel = new DefaultComboBoxModel<>(SPECTRUM_CHOICES);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Component getTableCellEditorComponent(final JTable table, final Object value, final boolean isSelected,
            final int row, final int column) {
        if (value instanceof Axis) {
            currentValue = (Axis) value;
            if (currentValue.isSpectrum()) {
                ((JComboBox<String>) editorComponent).setModel(spectrumComboBoxModel);
            } else {
                ((JComboBox<String>) editorComponent).setModel(imageComboBoxModel);
            }
            ((JComboBox) editorComponent).setSelectedItem(currentValue.toString());
        }
        return super.getTableCellEditorComponent(table, value, isSelected, row, column);
    }

    @Override
    public Object getCellEditorValue() {
        Axis newValue = null;
        if (editorComponent instanceof JComboBox) {
            Object item = ((JComboBox<?>) editorComponent).getSelectedItem();
            if ((item != null) && (currentValue != null)) {
                String axis = item.toString();
                newValue = currentValue.clone();
                newValue.setAxis(axis);
            }
        }
        return newValue;
    }
}
