package fr.soleil.bean.scanserver;

public interface ICurrentScanConfigurationBeanListener {

    public void actuatorChanged(String actuatorName);

    public void sensorsZChanged(String... zSensorsDataList);

    public void sensorsY1Changed(String... y1SensorsDataList);

    public void sensorsY2Changed(String... y1SensorsDataList);

    public void sensorsNoneChanged(String... noneSensorsDataList);

    public void fittedY1DataChanged(String attributeName);

    public void fittedY2DataChanged(String attributeName);

    public void fittedNoneDataChanged(String attributeName);

}
