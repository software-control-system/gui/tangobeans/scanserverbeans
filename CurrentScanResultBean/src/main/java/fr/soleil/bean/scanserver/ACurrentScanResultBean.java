package fr.soleil.bean.scanserver;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.WindowConstants;

public abstract class ACurrentScanResultBean<C extends ACurrentScanActuatorInput> extends JSplitPane {

    private static final long serialVersionUID = 391735443420790890L;

    protected final C actuatorInput;
    protected String scanServerDeviceName;

    protected ACurrentScanResultBean(boolean isSendSensor) {
        super();
        actuatorInput = generateActuatorInput(isSendSensor);
    }

    protected abstract C generateActuatorInput(boolean isSendSensor);

    public final void setConfirmation(final boolean confirmation) {
        actuatorInput.setConfirmation(confirmation);
    }

    public final boolean isConfirmation() {
        return actuatorInput.isConfirmation();
    }

    public final String getScanServerDeviceName() {
        return scanServerDeviceName;
    }

    protected abstract void clearGUI();

    protected abstract void actionAfterScanServerDeviceNameChange();

    public final void setScanServerDeviceName(final String name) {
        if ((scanServerDeviceName != null) && !scanServerDeviceName.equalsIgnoreCase(name)) {
            clearGUI();
        }
        if ((name != null) && !name.equalsIgnoreCase(scanServerDeviceName)) {
            scanServerDeviceName = name;
            actionAfterScanServerDeviceNameChange();
        }
        actuatorInput.setScanServerDeviceName(scanServerDeviceName);
    }

    protected final boolean isConfigurationValide() {
        boolean isValid = false;
        if ((scanServerDeviceName != null) && !scanServerDeviceName.isEmpty()) {
            isValid = true;
        }
        return isValid;
    }

    public static void runMain(ACurrentScanResultBean<?> bean) {
        JFrame frame = new JFrame(bean.getClass().getSimpleName() + ": " + bean.getScanServerDeviceName());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(bean);
        frame.pack();
        frame.setVisible(true);
    }

}
