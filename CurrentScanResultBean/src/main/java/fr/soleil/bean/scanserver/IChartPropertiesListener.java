package fr.soleil.bean.scanserver;

import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;

public interface IChartPropertiesListener {

    public void chartPropertiesChange(ChartProperties properties);

    public void plotPropertiesChange(String viewId, PlotProperties properties);

}
