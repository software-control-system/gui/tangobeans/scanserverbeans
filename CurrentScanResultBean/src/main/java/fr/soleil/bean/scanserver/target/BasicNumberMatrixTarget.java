package fr.soleil.bean.scanserver.target;

import fr.soleil.data.target.matrix.INumberMatrixTarget;
import fr.soleil.lib.project.math.ArrayUtils;

public class BasicNumberMatrixTarget extends BasicTarget implements INumberMatrixTarget {

    private int matrixHeight = 0;
    private int matrixWidth = 0;
    private Object flatMatrix = null;
    private Object[] matrix = null;

    @Override
    public int getMatrixDataHeight(Class<?> arg0) {
        return matrixHeight;
    }

    @Override
    public int getMatrixDataWidth(Class<?> arg0) {
        return matrixWidth;
    }

    @Override
    public boolean isPreferFlatValues(Class<?> arg0) {
        return false;
    }

    @Override
    public Object getFlatNumberMatrix() {
        return flatMatrix;
    }

    @Override
    public Object[] getNumberMatrix() {
        // TODO Basically, only getFlatNumberMatrix need to be used
        return matrix;
    }

    @Override
    public void setFlatNumberMatrix(Object matrix, int width, int height) {
        matrixHeight = height;
        matrixWidth = width;
        flatMatrix = matrix;
        notifyBasicListeners();
    }

    @Override
    public void setNumberMatrix(Object[] value) {
        matrix = value;
        Object flatValue = null;
        int width = 0, height = 0;
        if (value != null) {
            height = value.length;
            int[] shape = ArrayUtils.recoverShape(value);
            if (shape != null && shape.length == 2) {
                height = shape[0];
                width = shape[1];
                flatValue = ArrayUtils.convertArrayDimensionFromNTo1(value);
            }
        }
        setFlatNumberMatrix(flatValue, width, height);
    }

}
