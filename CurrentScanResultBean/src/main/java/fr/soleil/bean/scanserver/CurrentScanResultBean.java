/*******************************************************************************
 * Copyright (c) 2006 Synchrotron SOLEIL
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.All rights reserved. This program and the accompanying materials
 *
 * Contributors: katy.saintin@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/

/**
 * @author saintin
 */
package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.thread.IDrawingThreadManager;
import fr.soleil.data.target.IDrawableTarget;
import fr.soleil.data.target.scalar.ITextTarget;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;
import fr.soleil.model.scanserver.Axis;
import fr.soleil.model.scanserver.CurrentScanDataModel;
import fr.soleil.model.scanserver.IScanServerListener;

public class CurrentScanResultBean extends AbstractTangoBox
        implements ICurrentScanConfigurationBeanListener, IScanServerListener, PropertyChangeListener {

    private static final long serialVersionUID = -6384168508525804834L;

    public static final String PLAYER_MODE_LABEL = "View data into player";
    private static final String RUN_NAME = "Run name:";
    private static final String FILE = "File: ";

    private static final String SPECTRUM = "SPECTRUM";
    private static final String IMAGE = "IMAGE";

    // GUI Elements
    private final JPanel mainPanel;
    private final JLabel runLabel;
    private final Label runName;
    private final Label nexusFileName;
    private final ITextTarget nexusFileTarget;

    private final JPanel topPanel;
    private final JPanel labelPanel;

    private final JTabbedPane centerPanel;
    private CurrentScan2DResultBean scan2DBean;
    private CurrentScan1DResultBean scan1DBean;

    private String oldDate;
    private boolean listenDividerEvent;

    /**
     * The scan Server Device Name
     */
    private CurrentScanConfigurationBean currentScanConfigurationBean;

    public CurrentScanResultBean(boolean showImageAsSpectrumStack, String exportTrajectortDeviceName,
            boolean isSendSensor) {
        super();
        scan1DBean = new CurrentScan1DResultBean(showImageAsSpectrumStack, isSendSensor);
        scan2DBean = new CurrentScan2DResultBean(exportTrajectortDeviceName);
        mainPanel = new JPanel();
        runLabel = new JLabel(RUN_NAME);
        runName = new Label();
        nexusFileName = new Label();
        nexusFileTarget = generateNexusFileTarget();
        topPanel = new JPanel(new BorderLayout());
        labelPanel = new JPanel(new VerticalFlowLayout());
        centerPanel = new JTabbedPane();
        listenDividerEvent = true;
        initGUI();
    }

    private void initGUI() {
        setLayout(new BorderLayout());
        centerPanel.add(SPECTRUM, scan1DBean);
        centerPanel.add(IMAGE, scan2DBean);
        scan2DBean.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, this);
        scan1DBean.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, this);

        // init top panel
        initTopPanel();

        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(topPanel, BorderLayout.NORTH);
        mainPanel.add(centerPanel, BorderLayout.CENTER);
        add(mainPanel, BorderLayout.CENTER);
    }

    public void setManagementPanelVisible(boolean visible) {
        if (scan1DBean != null) {
            scan1DBean.setManagementPanelVisible(visible);
        }
    }

    public boolean isManagementPanelVisible() {
        boolean visible = false;
        if (scan1DBean != null) {
            visible = scan1DBean.isManagementPanelVisible();
        }
        return visible;
    }

    public void setDividerLocation(int location) {
        listenDividerEvent = false;
        scan2DBean.setDividerLocation(location);
        scan1DBean.setDividerLocation(location);
        listenDividerEvent = true;
    }

    @Override
    public void propertyChange(PropertyChangeEvent arg0) {
        if (listenDividerEvent) {
            JSplitPane source = (JSplitPane) arg0.getSource();
            dividerLocationChanged(source.getDividerLocation());
        }
    }

    protected void dividerLocationChanged(int location) {
    }

    private void initTopPanel() {
        labelPanel.add(runLabel);
        labelPanel.add(runName);
        labelPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        labelPanel.add(nexusFileName);
        topPanel.add(labelPanel, BorderLayout.NORTH);
    }

    public CurrentScanConfigurationBean getCurrentScanConfigurationBean() {
        return currentScanConfigurationBean;
    }

    public void setChartProperties(ChartProperties properties) {
        scan1DBean.setChartProperties(properties);
    }

    public void setPlotPropertiesMap(Map<String, PlotProperties> plotPropertiesMap) {
        scan1DBean.setPlotPropertiesMap(plotPropertiesMap);
    }

    public void setCurrentScanConfigurationBean(CurrentScanConfigurationBean currentScanConfigurationBean) {
        CurrentScanConfigurationBean formerScanConfigurationBean = this.currentScanConfigurationBean;
        if (formerScanConfigurationBean != null) {
            formerScanConfigurationBean.removeCurrentScanConfigurationBeanListener(this);
        }
        this.currentScanConfigurationBean = currentScanConfigurationBean;
        if (currentScanConfigurationBean != null) {
            setZSensorsDataList(currentScanConfigurationBean.getZSensorsDataList());
            setY1SensorsDataList(currentScanConfigurationBean.getY1SensorsDataList());
            setY2SensorsDataList(currentScanConfigurationBean.getY2SensorsDataList());
            setActuatorName(currentScanConfigurationBean.getActuatorName());
            fittedY2DataChanged(currentScanConfigurationBean.getFittedAttributeName());
            currentScanConfigurationBean.addCurrentScanConfigurationBeanListener(this);
        }
    }

    public void setConfirmation(boolean confirmation) {
        scan2DBean.setConfirmation(confirmation);
        scan1DBean.setConfirmation(confirmation);
    }

    public boolean isConfirmation() {
        return scan2DBean.isConfirmation();
    }

    public String getScanServerDeviceName() {
        return model;
    }

    public void setScanServerDeviceName(String name) {
        if (model != null && !model.equalsIgnoreCase(name)) {
            CurrentScanDataModel.removeScanServerListener(model, this);
            clearGUI();
        }
        if (name != null && !name.equalsIgnoreCase(model)) {
            setModel(name);
            CurrentScanDataModel.addScanServerListener(name, this);
        }
        scan2DBean.setScanServerDeviceName(name);
        scan1DBean.setScanServerDeviceName(name);
    }

    public Map<String, PlotProperties> getPlotPropertiesMap() {
        return scan1DBean.getPlotPropertiesMap();
    }

    public ChartProperties getChartProperties() {
        return scan1DBean.getChartProperties();
    }

    public Map<String, ImageProperties> getImagePropertiesMap() {
        return scan2DBean.getImagePropertiesMap();
    }

    public void setImagePropertiesMap(Map<String, ImageProperties> impagePropMap) {
        scan2DBean.setImagePropertiesMap(impagePropMap);
    }

    private void updateAxisList(final String[] attributeNameList, final String axis) {
        CurrentScanDataModel.setAttributeListAxis(getModel(), attributeNameList, axis);
    }

    public String[] getZSensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(getModel(), Axis.Z);
    }

    public void setZSensorsDataList(String... zSensorsDataList) {
        if (currentScanConfigurationBean == null) {
            updateAxisList(zSensorsDataList, Axis.Z);
        }
        scan2DBean.initAttributeList();
        if (zSensorsDataList != null && zSensorsDataList.length > 0) {
            centerPanel.setSelectedComponent(scan2DBean);
        } else {
            centerPanel.setSelectedComponent(scan1DBean);
        }
    }

    public String[] getY1SensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(getModel(), Axis.Y1);
    }

    public void setY1SensorsDataList(String... y1SensorsDataList) {
        if (currentScanConfigurationBean == null) {
            updateAxisList(y1SensorsDataList, Axis.Y1);
        }
        scan1DBean.initAttributeList();
    }

    public String[] getY2SensorsDataList() {
        return CurrentScanDataModel.buildAttributeList(getModel(), Axis.Y2);
    }

    public void setY2SensorsDataList(String... y2SensorsDataList) {
        if (currentScanConfigurationBean == null) {
            updateAxisList(y2SensorsDataList, Axis.Y2);
        }
        scan1DBean.initAttributeList();
    }

    public String getActuatorName() {
        return CurrentScanDataModel.getActuatorName(getModel());
    }

    public void setActuatorName(String actuatorName) {
        if (currentScanConfigurationBean == null) {
            CurrentScanDataModel.setActuatorName(getModel(), actuatorName);
        }
        scan1DBean.initAttributeList();
    }

    @Override
    public void actuatorChanged(String actuatorName) {
        setActuatorName(actuatorName);
    }

    @Override
    public void sensorsZChanged(String... zSensorsDataList) {
        setZSensorsDataList(zSensorsDataList);
    }

    @Override
    public void sensorsY1Changed(String... y1SensorsDataList) {
        setY1SensorsDataList(y1SensorsDataList);
    }

    @Override
    public void sensorsY2Changed(String... y2SensorsDataList) {
        setY2SensorsDataList(y2SensorsDataList);
    }

    @Override
    public void fittedY1DataChanged(String attributeName) {
        scan1DBean.fittedY1DataChanged(attributeName);
    }

    @Override
    public void fittedY2DataChanged(String attributeName) {
        scan1DBean.fittedY2DataChanged(attributeName);
    }

    @Override
    public void fittedNoneDataChanged(String attributeName) {
        scan1DBean.fittedNoneDataChanged(attributeName);
    }

    @Override
    public void sensorsNoneChanged(String... noneSensorsDataList) {
        // not managed
    }

    private ITextTarget generateNexusFileTarget() {
        return new NexusFileNameChanger();
    }

    @Override
    protected void refreshGUI() {
        TangoKey tangoKey = generateAttributeKey(CurrentScanDataModel.RUN_NAME);
        stringBox.setColorEnabled(runName, false);
        stringBox.connectWidget(runName, tangoKey);

        TangoKey tangokeyFileName = generateAttributeKey(CurrentScanDataModel.NEXUS_FILE);
        stringBox.connectWidget(nexusFileTarget, tangokeyFileName);
    }

    @Override
    protected void clearGUI() {
        cleanWidget(runName);
        cleanWidget(nexusFileTarget);
    }

    @Override
    public void stateChanged(String state) {
        if (CurrentScanDataModel.dateChanged(model, oldDate)) {
            beforeNewScan();
            oldDate = CurrentScanDataModel.getStartDate(model);
            CurrentScanDataModel.updateDataModel(model);
            scan2DBean.initAttributeList();
            scan1DBean.initAttributeList();
            scan1DBean.setChartProperties(CurrentScanDataModel.getChartPropertyScan(getModel()));
            afterNewScan();
        }
    }

    protected void beforeNewScan() {
    }

    protected void afterNewScan() {
    }

    @Override
    protected void onConnectionError() {
        // not managed
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not managed
    }

    public double[] generateScan2DTrajectories() {
        return scan2DBean.getTrajectories();
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        CurrentScanResultBean bean = new CurrentScanResultBean(false, null, false);
        bean.setScanServerDeviceName("ica/salsa/scan.1");
        bean.setZSensorsDataList("data_01", "data_02");

        bean.start();
        JFrame frame = new JFrame();
        frame.setContentPane(bean);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    // Must implement IDrawableTarget to be changed in EDT (SCAN-976, PROBLEM-2582)
    protected class NexusFileNameChanger implements ITextTarget, IDrawableTarget {

        @Override
        public void addMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public void removeMediator(Mediator<?> mediator) {
            // not managed
        }

        @Override
        public IDrawingThreadManager getDrawingThreadManager() {
            return EDTManager.INSTANCE;
        }

        @Override
        public String getText() {
            // not managed
            return null;
        }

        @Override
        public void setText(String file) {
            if (file != null && !file.isEmpty() && !StringScalarBox.DEFAULT_ERROR_STRING.equals(file)) {
                String fileName = file;
                int index = file.lastIndexOf(TangoDeviceHelper.SLASH);
                if (index > -1) {
                    fileName = file.substring(index + 1);
                }
                nexusFileName.setText(FILE + fileName);
                nexusFileName.setVisible(true);
                nexusFileName.setToolTipText(file);
            } else {
                nexusFileName.setText(ObjectUtils.EMPTY_STRING);
                nexusFileName.setVisible(false);
                nexusFileName.setToolTipText(ObjectUtils.EMPTY_STRING);
            }
        }

    }

}
