package fr.soleil.bean.scanserver;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceProxyFactory;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.event.ImageViewerEvent;
import fr.soleil.comete.definition.event.ImageViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IImageViewerListener;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.model.scanserver.Axis;
import fr.soleil.model.scanserver.CurrentScanDataModel;

public class CurrentScan2DResultBean extends ACurrentScanResultBean<CurrentScan2DActuatorInput>
        implements IImageViewerListener {

    private static final long serialVersionUID = -3308808382505195587L;

    private static final String PUBLISHER_TRAJECTORY = "trajectory";
    private static final String PUBLISHER_Y_ACTUATOR = "yActuator";
    private static final String PUBLISHER_X_ACTUATOR = "xActuator";
    public static final String IMAGE_PLAYER_ID = "Player";

    // GUI Element
    private final NumberMatrixBox imageBox;
    private final JPanel imagePanel;
    private final JPanel axisPanel;
    private final JTabbedPane tabbedPane;

    private String xAttributeName;
    private String yAttributeName;
    private final JComboBox<String> xCombo;
    private final JComboBox<String> yCombo;

    private final ArrayPositionConvertor xValueConvertor;
    private final ArrayPositionConvertor yValueConvertor;

    // <AttributeName,ImageViewer>
    private final Map<String, ImageViewer> imageViewerMap;
    private final Map<String, ImageProperties> imageProp;

    /**
     * The scan Server Device Name
     */

    public CurrentScan2DResultBean(String exportTrajectortDeviceName) {
        super(ACurrentScanActuatorInput.DEFAULT_SEND_SENSOR);

        xAttributeName = ObjectUtils.EMPTY_STRING;
        yAttributeName = ObjectUtils.EMPTY_STRING;
        xValueConvertor = new ArrayPositionConvertor();
        yValueConvertor = new ArrayPositionConvertor();
        imageViewerMap = new HashMap<>();
        imageProp = new HashMap<>();

        imageBox = new NumberMatrixBox();
        imagePanel = new JPanel();
        axisPanel = new JPanel();
        tabbedPane = new JTabbedPane();
        xCombo = new JComboBox<>();
        yCombo = new JComboBox<>();
        initializeGUI(exportTrajectortDeviceName);
    }

    @Override
    protected CurrentScan2DActuatorInput generateActuatorInput(boolean isSendSensor) {
        return new CurrentScan2DActuatorInput(isSendSensor);
    }

    /**
     * Initialization.
     */
    private void initializeGUI(String exportTrajectortDeviceName) {
        setOneTouchExpandable(true);
        setOrientation(JSplitPane.VERTICAL_SPLIT);
        imagePanel.setLayout(new BorderLayout());
        imagePanel.add(tabbedPane, BorderLayout.CENTER);
        imagePanel.add(axisPanel, BorderLayout.NORTH);

        axisPanel.add(new JLabel("X actuator : "));
        axisPanel.add(xCombo);
        axisPanel.add(new JLabel("Y actuator : "));
        axisPanel.add(yCombo);
        JButton exportTrajectory = new JButton(createExportAction(exportTrajectortDeviceName, this));
        if (exportTrajectortDeviceName != null) {
            if (!exportTrajectortDeviceName.trim().isEmpty()) {
                axisPanel.add(exportTrajectory);
            }
        }

        xCombo.setEditable(false);
        yCombo.setEditable(false);

        xCombo.addActionListener((e) -> {
            String actionCommand = e.getActionCommand();
            if (e.getModifiers() == InputEvent.BUTTON1_MASK || !actionCommand.equals("comboBoxChanged")) {
                setXAttributeName(xCombo.getSelectedItem().toString());
            }
        });

        yCombo.addActionListener((e) -> {
            String actionCommand = e.getActionCommand();
            if (e.getModifiers() == InputEvent.BUTTON1_MASK || !actionCommand.equals("comboBoxChanged")) {
                setYAttributeName(yCombo.getSelectedItem().toString());
            }
        });

        setTopComponent(imagePanel);
        setBottomComponent(actuatorInput);
    }

    private Action createExportAction(final String exportTrajectortDeviceName, final CurrentScan2DResultBean bean) {
        return new TrajectoryExportAction(exportTrajectortDeviceName, bean);
    }

    public Map<String, ImageProperties> getImagePropertiesMap() {
        imageProp.clear();
        if ((imageViewerMap != null) && (!imageViewerMap.isEmpty())) {
            Set<String> imageViewerKeys = imageViewerMap.keySet();
            ImageViewer imageViewer = null;
            for (String attributeName : imageViewerKeys) {
                imageViewer = imageViewerMap.get(attributeName);
                imageProp.put(imageViewer.getImageName().toLowerCase(), imageViewer.getImageProperties());
            }
        }
        return imageProp;
    }

    public void setImagePropertiesMap(final Map<String, ImageProperties> impagePropMap) {
        imageProp.clear();
        if (impagePropMap != null && !impagePropMap.isEmpty()) {
            imageProp.putAll(impagePropMap);
        }
        refreshImagePropertiesMap();
    }

    private void refreshImagePropertiesMap() {
        if ((imageProp != null) && (!imageProp.isEmpty()) && (imageViewerMap != null) && (!imageViewerMap.isEmpty())) {
            Collection<ImageViewer> imageViewers = imageViewerMap.values();
            ImageProperties properties = null;
            for (ImageViewer imageViewer : imageViewers) {
                properties = imageProp.get(imageViewer.getImageName().toLowerCase());
                if (properties != null) {
                    imageViewer.setImageProperties(properties);
                }
            }
        }
    }

    @Override
    protected void actionAfterScanServerDeviceNameChange() {
        refreshImageViewerBuild(false);
    }

    private void buildImageViewerMap() {
        clearGUI();
        String[] zSensorsDataList = CurrentScanDataModel.buildAttributeList(scanServerDeviceName, Axis.Z);
        if (zSensorsDataList != null) {
            for (String element : zSensorsDataList) {
                ImageViewer imageViewer = new ImageViewer();
                imageViewer.setApplicationId(element);
                imageViewer.setAlwaysFitMaxSize(true);
                imageViewer.setCleanOnDataSetting(false);
                imageViewerMap.put(element, imageViewer);
                EDTManager.INSTANCE.runInDrawingThread(() -> {
                    tabbedPane.add(element, imageViewer);
                });
                imageViewer.addImageViewerListener(this);
            }
        }
    }

    public void initAttributeList() {
        if (isConfigurationValide()) {
            buildImageViewerMap();
            TangoKey imageKey = null;
            String label = null;
            // build TangoKey and register Key
            for (Entry<String, ImageViewer> entry : imageViewerMap.entrySet()) {
                String key = entry.getKey();
                final ImageViewer imageViewer = entry.getValue();
                imageKey = new TangoKey();
                TangoKeyTool.registerAttribute(imageKey, scanServerDeviceName, key);
                label = CurrentScanDataModel.getAttributeLabel(scanServerDeviceName, key);
                imageViewer.setImageName(label);
                imageBox.setSynchronTransmission(imageViewer, true);
                imageBox.connectWidget(imageViewer, imageKey);
                setRefreshingPeriod(imageKey);
                setXAttributeName(xAttributeName);
                setYAttributeName(yAttributeName);
                if (imageProp != null && label != null) {
                    final ImageProperties property = imageProp.get(label.toLowerCase());
                    if (property != null) {
                        imageViewer.setImageProperties(property);
                    }
                }
                imageBox.setSynchronTransmission(imageViewer, false);
            }
            refreshComboBox();
        }
    }

    public String getXAttributeName() {
        return xAttributeName;
    }

    public void setXAttributeName(final String xAttributeName) {
        this.xAttributeName = xAttributeName;
        if (TangoAttributeHelper.isAttributeRunning(getScanServerDeviceName(), xAttributeName)) {
            TangoKey xKey = new TangoKey();
            setRefreshingPeriod(xKey);
            TangoKeyTool.registerAttribute(xKey, getScanServerDeviceName(), xAttributeName);
            setAxisKey(xKey, true);
        } else {
            setAxisKey(null, true);
        }

    }

    public String getYAttributeName() {
        return yAttributeName;
    }

    public void setYAttributeName(final String yAttributeName) {
        this.yAttributeName = yAttributeName;
        if (TangoAttributeHelper.isAttributeRunning(getScanServerDeviceName(), yAttributeName)) {
            TangoKey yKey = new TangoKey();
            setRefreshingPeriod(yKey);
            TangoKeyTool.registerAttribute(yKey, getScanServerDeviceName(), yAttributeName);
            setAxisKey(yKey, false);
        } else {
            setAxisKey(null, false);
        }
    }

    private void setAxisKey(final IKey key, final boolean xAxis) {
        for (ImageViewer imageViewer : imageViewerMap.values()) {
            if (imageViewer != null) {
                if (xAxis) {
                    imageBox.disconnectWidgetFromAll(xValueConvertor);
                    if (key != null) {
                        imageBox.connectWidget(xValueConvertor, key);
                    }
                    imageViewer.setXAxisConvertor(xValueConvertor);

                } else {
                    imageBox.disconnectWidgetFromAll(yValueConvertor);
                    if (key != null) {
                        imageBox.connectWidget(yValueConvertor, key);
                    }
                    imageViewer.setYAxisConvertor(yValueConvertor);
                }
            }
        }
    }

    public void addImageViewerListener(final IImageViewerListener listener) {
        if (listener != null) {
            for (ImageViewer imageViewer : imageViewerMap.values()) {
                if (imageViewer != null) {
                    imageViewer.addImageViewerListener(listener);
                }
            }
        }
    }

    public void removeImageViewerListener(final IImageViewerListener listener, final boolean withDisconnect) {
        for (ImageViewer imageViewer : imageViewerMap.values()) {
            imageViewer.removeImageViewerListener(listener);
            if (withDisconnect) {
                imageBox.disconnectWidgetFromAll(imageViewer);
            }
        }
    }

    private void refreshImageViewerBuild(final boolean withNewBuild) {
        if (withNewBuild) {
            buildImageViewerMap();
        }
        initAttributeList();
    }

    @Override
    protected void clearGUI() {
        removeImageViewerListener(this, true);
        imageViewerMap.clear();
        imageBox.disconnectWidgetFromAll(yValueConvertor);
        imageBox.disconnectWidgetFromAll(xValueConvertor);
        // SCAN-896: call this in EDT.
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            tabbedPane.removeAll();
            clearComboBox();
        });
    }

    private void clearComboBox() {
        xCombo.removeAllItems();
        yCombo.removeAllItems();
        xCombo.setEnabled(false);
        yCombo.setEnabled(false);
    }

    public void refreshComboBox() {
        List<String> xAttributeList = new ArrayList<>();
        List<String> yAttributeList = new ArrayList<>();

        String[] sensorsList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                CurrentScanDataModel.SENSORS_DATA_LIST);

        String[] tmpXActuatorsDataList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                CurrentScanDataModel.ACTUATORS_DATA_LIST);

        if (tmpXActuatorsDataList != null) {
            for (String attributeName : tmpXActuatorsDataList) {
                xAttributeList.add(attributeName);
            }
        }

        String[] tmpYActuatorsDataList = CurrentScanDataModel.readAttributeList(scanServerDeviceName,
                CurrentScanDataModel.YACTUATORS_DATA_LIST);
        if (tmpYActuatorsDataList != null) {
            for (String attributeName : tmpYActuatorsDataList) {
                yAttributeList.add(attributeName);
            }
        }

        if (sensorsList != null) {
            for (String attributeName : sensorsList) {
                xAttributeList.add(attributeName);
                yAttributeList.add(attributeName);
            }
        }

        // SCAN-896: call this in EDT.
        EDTManager.INSTANCE.runInDrawingThread(() -> {
            clearComboBox();
            if (xCombo.getItemCount() == 0) {
                xCombo.addItem(ObjectUtils.EMPTY_STRING);
                for (String xItem : xAttributeList) {
                    xCombo.addItem(xItem);
                }
                xCombo.setEnabled(xAttributeList.size() > 1);
            }

            if (yCombo.getItemCount() == 0) {
                yCombo.addItem(ObjectUtils.EMPTY_STRING);
                for (String yItem : yAttributeList) {
                    yCombo.addItem(yItem);
                }
                yCombo.setEnabled(yAttributeList.size() > 1);
            }
            xCombo.repaint();
            yCombo.repaint();
        });
    }

    @Override
    public void imageViewerChanged(final ImageViewerEvent event) {
        if (event != null) {
            if (event.getReason() == Reason.SELECTION) {
                double[] point = event.getSelectedPoint();
                if (point != null && point.length == 2) {
                    int x = Double.valueOf(point[0]).intValue();
                    int y = Double.valueOf(point[1]).intValue();
                    actuatorInput.setActuatorIndex(x, y);
                }
            }
        }
    }

    private void setRefreshingPeriod(final IKey key) {
        int refreshingPeriod = CurrentScanDataModel.getIntegrationTime(scanServerDeviceName);
        // DO NOT POLLED TO FAST => TIME OUT ERROR BUG 23612
        if (refreshingPeriod < 1000) {
            refreshingPeriod = 1000;
        }
        if (refreshingPeriod != TangoDataSourceFactory.DEFAULT_SLEEPING_PERIOD) {
            IDataSourceProducer producer = DataSourceProducerProvider
                    .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
            if ((producer instanceof AbstractRefreshingManager<?>) && (key != null)) {
                ((AbstractRefreshingManager<?>) producer).setRefreshingStrategy(key,
                        new PolledRefreshingStrategy(refreshingPeriod));
            }
        }
    }

    // return true if there is at least one image
    public boolean hasData() {
        boolean hasData = false;
        if (imageViewerMap != null) {
            hasData = !imageViewerMap.isEmpty();
        }
        return hasData;
    }

    public double[] getTrajectories() {
        double[] result = null;
        ImageViewer viewer = (ImageViewer) tabbedPane.getSelectedComponent();
        if (viewer != null) {
            if (viewer.getYAxisConvertor() == null || viewer.getXAxisConvertor() == null) {
                JOptionPane.showMessageDialog(this,
                        "The trajectory can not be computed since you have not specified the actuators",
                        "Trajectories error", JOptionPane.ERROR_MESSAGE);
                return null;
            }
            result = viewer.getTrajectories();
        }
        return result;
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        CurrentScan2DResultBean bean = new CurrentScan2DResultBean(null);
        bean.setScanServerDeviceName("ica/salsa/scan.1");
        bean.initAttributeList();
        runMain(bean);
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected class TrajectoryExportAction extends AbstractAction {

        private static final long serialVersionUID = 4938884084145948570L;

        protected final String exportTrajectortDeviceName;
        protected final CurrentScan2DResultBean bean;

        public TrajectoryExportAction(String exportTrajectortDeviceName, CurrentScan2DResultBean bean) {
            super("Export Trajectory");
            this.exportTrajectortDeviceName = exportTrajectortDeviceName;
            this.bean = bean;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            double[] traj = getTrajectories();
            String xActuator = getXAttributeName();
            xActuator = TangoAttributeHelper.getLabel(scanServerDeviceName, xActuator);
            String yActuator = getYAttributeName();
            yActuator = TangoAttributeHelper.getLabel(scanServerDeviceName, yActuator);
            if (xActuator == null || yActuator == null) {
                JOptionPane.showMessageDialog(bean,
                        "The trajectory can not be computed since you have not specified the actuators",
                        "Trajectory error", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    if (traj != null) {
                        DeviceProxy dp = DeviceProxyFactory.get(exportTrajectortDeviceName);
                        DeviceAttribute dAx = new DeviceAttribute(PUBLISHER_X_ACTUATOR, xActuator);
                        DeviceAttribute dAy = new DeviceAttribute(PUBLISHER_Y_ACTUATOR, yActuator);
                        DeviceAttribute dTraj = new DeviceAttribute(PUBLISHER_TRAJECTORY, traj, traj.length, 1);
                        dp.write_attribute(dAx);
                        dp.write_attribute(dAy);
                        dp.write_attribute(dTraj);
                    } else {
                        JOptionPane.showMessageDialog(bean,
                                "Please define a trajectory on the image (righ-click on image -> Selection mode ->  Point ROI or Line ROI)",
                                "Trajectory error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (DevFailed df) {
                    JOptionPane.showMessageDialog(bean, DevFailedUtils.toString(df));
                }
            }
        }

    }

}
