package fr.soleil.bean.scanserver.target;

public interface IBasicTargetListener {

    public void onUpdate(BasicTarget updatedTarget);
}
