package fr.soleil.bean.scanserver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.CurveProperties;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.chart.data.DataView;
import fr.soleil.comete.swing.chart.data.DataViewList;
import fr.soleil.comete.swing.chart.util.ShowInFrameMenu;
import fr.soleil.comete.swing.util.EDTManager;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoAttributeHelper;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.model.scanserver.Axis;
import fr.soleil.model.scanserver.CurrentScanDataModel;

public class CurrentScan1DResultBean extends ACurrentScanResultBean<CurrentScan1DActuatorInput>
        implements IChartViewerListener {

    private static final long serialVersionUID = 1589489656780953622L;

    private static final CometeColor[] COLOR_ARRAY = new CometeColor[] { CometeColor.BLUE, CometeColor.RED,
            CometeColor.GREEN, CometeColor.ORANGE, CometeColor.CYAN, CometeColor.MAGENTA, CometeColor.PINK,
            CometeColor.DARK_GRAY, CometeColor.BLACK };

    private TangoKey xTangoKey;

    // GUI ELEMENTS
    private final Chart chartViewer;
    private final ChartViewerBox chartBox;

    private String dataFittedName;
    private int dataFittedAxis;
    private final CometeColor dataFittedColor;
    private final Map<String, PlotProperties> plotPropertiesMap;
    private ChartProperties chartProperties;
    private ChartProperties defaultChartProp;

    private boolean showImageAsSpectrumStack;

    public CurrentScan1DResultBean(boolean showImageAsSpectrumStack, boolean isSendSensor) {
        super(isSendSensor);
        chartViewer = new Chart();
        chartBox = new ChartViewerBox();
        dataFittedAxis = IChartViewer.Y1;
        dataFittedColor = new CometeColor(128, 64, 0);
        plotPropertiesMap = new HashMap<>();
        this.showImageAsSpectrumStack = showImageAsSpectrumStack;
        initGUI();
    }

    @Override
    protected CurrentScan1DActuatorInput generateActuatorInput(boolean isSendSensor) {
        return new CurrentScan1DActuatorInput(isSendSensor);
    }

    private void initGUI() {
        setOneTouchExpandable(true);
        setOrientation(JSplitPane.VERTICAL_SPLIT);

        // Chart
        setTopComponent(chartViewer);
        ShowInFrameMenu showInFrameMenu = new ShowInFrameMenu();
        chartViewer.addMenuItem(showInFrameMenu);
        chartViewer.setUseDisplayNameForDataSaving(true);
        chartViewer.setDataViewsSortedOnX(false);
        chartViewer.setManagementPanelVisible(false);
        chartViewer.setCustomCometeColor(COLOR_ARRAY);
        chartViewer.setMathExpressionEnabled(true);
        chartViewer.setCyclingCustomMap(true);
        chartViewer.setCleanDataViewConfigurationOnRemoving(false);
        chartViewer.setAutoHighlightOnLegend(true);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y1);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.Y2);
        chartViewer.setGridStyle(IChartViewer.STYLE_DASH, IChartViewer.X);
        defaultChartProp = chartViewer.getChartProperties();
        // Actuator
        setBottomComponent(actuatorInput);
    }

    public void setManagementPanelVisible(boolean visible) {
        if (chartViewer != null) {
            chartViewer.setManagementPanelVisible(false);
        }
    }

    public boolean isManagementPanelVisible() {
        boolean visible = false;
        if (chartViewer != null) {
            visible = chartViewer.isManagementPanelVisible();
        }
        return visible;
    }

    @Override
    protected void actionAfterScanServerDeviceNameChange() {
        initAttributeList();
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        if (event != null) {
            switch (event.getReason()) {
                case SELECTION:
                    int index = event.getSelectedIndex();
                    actuatorInput.setIndex(index);
                    break;

                case CONFIGURATION:
                    String viewId = event.getViewId();
                    if ((viewId == null) || viewId.isEmpty()
                            || viewId.startsWith(IChartViewer.CHART_PROPERTY_ACTION_NAME)) {
                        ChartProperties newProperties = chartViewer.getChartProperties();
                        chartProperties = newProperties;
                        CurrentScanDataModel.setChartPropertyScan(scanServerDeviceName, newProperties);
                    }
                    break;
                default:
                    break;

            }
        }
    }

    public void fittedY1DataChanged(String attributeName) {
        fittedY1DataChanged(attributeName, false);
    }

    public void fittedY1DataChanged(String attributeName, boolean userEvent) {
        dataFittedName = attributeName;
        dataFittedAxis = IChartViewer.Y1;
        initAttributeList();
    }

    public void fittedY2DataChanged(String attributeName) {
        fittedY2DataChanged(attributeName, false);
    }

    public void fittedY2DataChanged(String attributeName, boolean userEvent) {
        dataFittedName = attributeName;
        dataFittedAxis = IChartViewer.Y2;
        initAttributeList();
    }

    public void fittedNoneDataChanged(String attributeName) {
        if (dataFittedName != null) {
            dataFittedName = null;
            initAttributeList();
        }
    }

    private void initFittedDataList() {
        if (dataFittedName != null) {
            String deviceName = TangoDeviceHelper.getDeviceName(dataFittedName);
            String entityName = TangoDeviceHelper.getEntityName(dataFittedName);
            initAttributeList(deviceName, new String[] { entityName }, dataFittedAxis);
        }
    }

    private void updateChartInformations() {
        if (chartViewer != null) {
            ChartProperties prop = chartProperties == null ? defaultChartProp : chartProperties;

            chartViewer.removeChartViewerListener(this);
            chartViewer.setChartProperties(prop);
            chartViewer.addChartViewerListener(this);

            String actuatorName = CurrentScanDataModel.getActuatorName(scanServerDeviceName);
            if ((actuatorName != null) && actuatorName.endsWith("Timestamps")) {
                chartViewer.setAxisLabelFormat(IChartViewer.TIME_FORMAT, IChartViewer.X);
            } else {
                chartViewer.setAxisLabelFormat(IChartViewer.AUTO_FORMAT, IChartViewer.X);
            }
            updateAxisName();
        }
    }

    public void initAttributeList() {
        clearGUI();
        if (isConfigurationValide()) {
            initAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.buildAttributeList(scanServerDeviceName, Axis.Y1), IChartViewer.Y1);
            initAttributeList(scanServerDeviceName,
                    CurrentScanDataModel.buildAttributeList(scanServerDeviceName, Axis.Y2), IChartViewer.Y2);
            initFittedDataList();
        }
        updateChartInformations();
    }

    private void initAttributeList(String deviceName, String[] attributeList, int axis) {
        if ((attributeList != null) && (attributeList.length > 0)) {
            boolean column = false;
            String actuatorName = CurrentScanDataModel.getActuatorName(scanServerDeviceName);
            if ((actuatorName != null) && !actuatorName.isEmpty()) {
                column = (actuatorName.indexOf("actuator_2") > -1);
            }

            if ((actuatorName != null) && !actuatorName.isEmpty()) {
                xTangoKey = new TangoKey();
                TangoKeyTool.registerAttribute(xTangoKey, scanServerDeviceName, actuatorName);
            }

            int nbDataView;
            for (int i = 0; i < attributeList.length; i++) {
                String sensor = attributeList[i];
                if (column) {
                    nbDataView = CurrentScanDataModel.getAttributeWidth(deviceName, sensor);
                } else {
                    nbDataView = CurrentScanDataModel.getAttributeHeight(deviceName, sensor);
                }
                if (showImageAsSpectrumStack || nbDataView == 1) {
                    connectAttribute(deviceName, sensor, axis);
                } else if (!showImageAsSpectrumStack && nbDataView > 1) {
                    EDTManager.INSTANCE.runInDrawingThread(() -> {
                        JOptionPane.showMessageDialog(this,
                                "Incorrect display configuration:\nYou can't display images on the spectrum viewer when the option \"Allow display image as spectrum stack\" is disabled in the Preferences",
                                "Display Warning", JOptionPane.WARNING_MESSAGE);
                    });
                }
            }
        }
    }

    private void updateAxisName() {
        chartViewer.setAxisName("Index", IChartViewer.X);
        if (xTangoKey != null) {
            setRefreshingPeriod(xTangoKey);
            String actuatorName = CurrentScanDataModel.getActuatorName(scanServerDeviceName);
            String xAxisName = CurrentScanDataModel.getAttributeLabel(scanServerDeviceName, actuatorName);
            if (xAxisName != null) {
                chartViewer.setAxisName(xAxisName, IChartViewer.X);
            }
        }
        chartViewer.setDataViewsSortedOnX(false);
    }

    private void connectAttribute(String deviceName, String attribute, int axis) {
        IKey dataKey = null;
        TangoKey yTangoKey = new TangoKey();
        TangoKeyTool.registerAttribute(yTangoKey, deviceName, attribute);
        if (xTangoKey != null) {
            // Connect Widget to the source in dual
            String attributeName = TangoKeyTool.getAttributeName(xTangoKey);
            boolean column = (attributeName.indexOf("actuator_2") > -1);
            dataKey = ChartViewerBox.createDualKey(xTangoKey, yTangoKey);
            chartBox.setSplitMatrixThroughColumns(dataKey, column);
        } else {
            // Connect Widget to the source
            dataKey = yTangoKey;
        }
        setRefreshingPeriod(yTangoKey);
        if (dataKey != null) {
            chartBox.connectWidget(chartViewer, dataKey);
        }
        // Set on the desired axis information Key is equal to the dataView id
        String dataViewId = yTangoKey.getInformationKey();
        setDataViewConfiguration(dataViewId, axis, deviceName, attribute, -1);
    }

    private void setRefreshingPeriod(IKey key) {
        int refreshingPeriod = CurrentScanDataModel.getIntegrationTime(scanServerDeviceName);
        // DO NOT POLLED TO FAST => TIME OUT ERROR BUG 23612
        if (refreshingPeriod < 1000) {
            refreshingPeriod = 1000;
        }
        if (refreshingPeriod != TangoDataSourceFactory.DEFAULT_SLEEPING_PERIOD) {
            IDataSourceProducer producer = DataSourceProducerProvider
                    .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
            if ((producer instanceof AbstractRefreshingManager<?>) && (key != null)) {
                ((AbstractRefreshingManager<?>) producer).setRefreshingStrategy(key,
                        new PolledRefreshingStrategy(refreshingPeriod));
            }
        }
    }

    private void setDataViewConfiguration(String dataViewId, int axis, String deviceName, String attributeName,
            int elementIndex) {

        String displayName = CurrentScanDataModel.readAttributeLabel(deviceName, attributeName);

        // If it is a Image add _row to the dataViewId
        int nbDataView = getNbDataView(deviceName, attributeName);
        if (nbDataView > 0) {
            if (elementIndex > 0) {
                nbDataView = 1;
            }
            for (int i = 0; i < nbDataView; i++) {
                String newdataViewId = dataViewId;
                String newDisplayName = displayName;
                if (nbDataView > 1) {
                    newdataViewId = getDataViewExtendedName(dataViewId, i);
                    newDisplayName = getDataViewExtendedName(displayName, i);
                } else if (elementIndex > 0) {
                    newdataViewId = getDataViewExtendedName(dataViewId, elementIndex);
                    newDisplayName = getDataViewExtendedName(displayName, elementIndex);
                }

                PlotProperties properties = plotPropertiesMap.get(displayName);
                setDataViewConfiguration(newdataViewId, properties);
                chartViewer.setDataViewAxis(newdataViewId, axis);
                chartViewer.setDataViewDisplayName(newdataViewId, newDisplayName);
            }
        }
    }

    private int getNbDataView(String deviceName, String attributeName) {
        boolean column = false;
        String actuatorName = CurrentScanDataModel.getActuatorName(scanServerDeviceName);
        if ((actuatorName != null) && !actuatorName.isEmpty()) {
            column = (actuatorName.indexOf("actuator_2") > -1);
        }

        int nbDataView = 0;
        if (column) {
            nbDataView = CurrentScanDataModel.getAttributeWidth(deviceName, attributeName);
        } else {
            nbDataView = CurrentScanDataModel.getAttributeHeight(deviceName, attributeName);
        }
        return nbDataView;
    }

    private void setDataViewConfiguration(final String dataViewId, final PlotProperties properties) {
        if ((dataViewId != null) && (properties != null) && (chartViewer != null)) {
            // Do not modified the label
            // CurveProperties
            CurveProperties curveProp = properties.getCurve();
            chartViewer.setDataViewCometeColor(dataViewId, curveProp.getColor());
            chartViewer.setDataViewLineStyle(dataViewId, curveProp.getLineStyle());
            chartViewer.setDataViewLineWidth(dataViewId, curveProp.getWidth());
            chartViewer.setDataViewFillStyle(dataViewId, properties.getBar().getFillStyle());
            chartViewer.setDataViewFillMethod(dataViewId, properties.getBar().getFillingMethod());

            chartViewer.setDataViewMarkerProperties(dataViewId, properties.getMarker());
            chartViewer.setDataViewBarProperties(dataViewId, properties.getBar());
            chartViewer.setDataViewInterpolationProperties(dataViewId, properties.getInterpolation());
            chartViewer.setDataViewMathProperties(dataViewId, properties.getMath());
            chartViewer.setDataViewSmoothingProperties(dataViewId, properties.getSmoothing());
            chartViewer.setDataViewTransformationProperties(dataViewId, properties.getTransform());
        } else {
            CometeColor color = chartViewer.getDataViewCometeColor(dataViewId);
            if (color == null) {
                color = CometeColor.RED;
                if (dataViewId.equals(dataFittedName)) {
                    color = dataFittedColor;
                }
            }
            chartViewer.setDataViewCometeColor(dataViewId, color);
            chartViewer.setDataViewLineStyle(dataViewId, IChartViewer.STYLE_SOLID);
            chartViewer.setDataViewLineWidth(dataViewId, 1);
            chartViewer.setDataViewMarkerStyle(dataViewId, IChartViewer.MARKER_DOT);
            chartViewer.setDataViewMarkerSize(dataViewId, 5);
            chartViewer.setDataViewMarkerCometeColor(dataViewId, color);
        }
    }

    private String getDataViewExtendedName(String original, int index) {
        return (original + "_" + index);
    }

    public ChartProperties getChartProperties() {
        ChartProperties currentChartProperties = null;
        if (chartViewer != null) {
            currentChartProperties = chartViewer.getChartProperties();
        }
        return currentChartProperties;
    }

    public void setChartProperties(ChartProperties properties) {
        chartProperties = properties;
        updateChartInformations();
    }

    public void setPlotPropertiesMap(Map<String, PlotProperties> aplotPropertiesMap) {
        plotPropertiesMap.clear();
        if (aplotPropertiesMap != null) {
            plotPropertiesMap.putAll(aplotPropertiesMap);
        }
        if ((chartViewer != null) && (scanServerDeviceName != null)) {
            String dataViewName = null;
            String dataViewId = null;
            PlotProperties property = null;
            for (Map.Entry<String, PlotProperties> entry : plotPropertiesMap.entrySet()) {
                dataViewName = entry.getKey();
                dataViewId = getDataViewIdForDisplayName(dataViewName);
                property = entry.getValue();
                if ((dataViewId != null) && (property != null)) {
                    setDataViewConfiguration(dataViewId, property);
                }
            }
        }
    }

    private String getDataViewIdForDisplayName(String displayName) {
        String dataViewId = null;
        if ((chartViewer != null) && (displayName != null)) {
            DataViewList dataViews = chartViewer.getDataViewList();
            if ((dataViews != null) && !dataViews.isEmpty()) {
                List<DataView> dataViewList = dataViews.getListCopy();
                for (DataView dataView : dataViewList) {
                    if (dataView.getDisplayName().toLowerCase().equalsIgnoreCase(displayName)) {
                        dataViewId = dataView.getId();
                        break;
                    }
                }
            }
        }
        return dataViewId;
    }

    public Map<String, PlotProperties> getPlotPropertiesMap() {
        plotPropertiesMap.clear();
        if (chartViewer != null) {
            DataViewList dataViews = chartViewer.getDataViewList();
            if ((dataViews != null) && !dataViews.isEmpty()) {
                List<DataView> dataViewList = dataViews.getListCopy();
                String dataViewId = null;
                String displayName = null;
                String deviceName = null;
                String attributeName = null;
                PlotProperties property = null;
                for (DataView dataView : dataViewList) {
                    dataViewId = dataView.getId();
                    displayName = dataView.getDisplayName();
                    // get deviceName and attributename
                    deviceName = TangoDeviceHelper.getDeviceName(dataViewId);
                    attributeName = TangoDeviceHelper.getEntityName(dataViewId);

                    // Only notified for spectrum and not for Image
                    if (TangoAttributeHelper.isAttributeRunning(deviceName, attributeName)) {
                        property = chartViewer.getDataViewPlotProperties(dataViewId);
                        if (property != null) {
                            plotPropertiesMap.put(displayName, property);
                        }
                    }
                }
            }
        }
        return plotPropertiesMap;
    }

    @Override
    public void clearGUI() {
        xTangoKey = null;
        chartBox.disconnectWidgetFromAll(chartViewer);
    }

    /**
     * Main method launches the application.
     * 
     * @param args
     */
    public static void main(final String[] args) {
        String scanServer = "ica/salsa/scan.2";
        if ((args != null) && (args.length > 0)) {
            scanServer = args[0];
        }
        CurrentScan1DResultBean bean = new CurrentScan1DResultBean(true, false);
        bean.setScanServerDeviceName(scanServer);
        runMain(bean);
    }
}
