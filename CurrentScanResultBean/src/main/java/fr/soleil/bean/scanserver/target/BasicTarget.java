package fr.soleil.bean.scanserver.target;

import java.util.ArrayList;
import java.util.List;

import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.TargetDelegate;

public class BasicTarget implements ITarget {

    private final TargetDelegate delegate = new TargetDelegate();
    private List<IBasicTargetListener> listeners = new ArrayList<IBasicTargetListener>();

    @Override
    public void addMediator(Mediator<?> mediator) {
        delegate.addMediator(mediator);
    }

    @Override
    public void removeMediator(Mediator<?> mediator) {
        delegate.removeMediator(mediator);
    }

    public void addTargetListener(IBasicTargetListener listener) {
        listeners.add(listener);
    }

    public void removeTargetListener(IBasicTargetListener listener) {
        listeners.remove(listener);
    }

    protected void notifyBasicListeners() {
        for (IBasicTargetListener listener : listeners) {
            listener.onUpdate(this);
        }
    }
}
