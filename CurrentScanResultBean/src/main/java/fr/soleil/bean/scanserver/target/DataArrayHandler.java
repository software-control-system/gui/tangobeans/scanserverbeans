package fr.soleil.bean.scanserver.target;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.soleil.comete.definition.data.target.complex.IDataArrayTarget;

public class DataArrayHandler extends BasicTarget implements IDataArrayTarget {

    private Map<String, Object> data = null;

    @Override
    public Map<String, Object> getData() {
        return data;
    }

    @Override
    public void setData(Map<String, Object> data) {
        if (this.data != data) {
            this.data = data;
            notifyBasicListeners();
        }
    }

    public int size() {
        return data.size();
    }

    @SuppressWarnings("unchecked")
    public Entry<String, Object> getEntryForIndex(int index) {
        Entry<String, Object> result = null;
        Set<Entry<String, Object>> setEntry = data.entrySet();
        if (index >= 0 && index < setEntry.size()) {
            Object[] listEntry = setEntry.toArray();
            result = (Entry<String, Object>) listEntry[index];
        }
        return result;
    }

    @Override
    public void addData(Map<String, Object> dataToAdd) {
        setData(dataToAdd);
    }

    @Override
    public void removeData(Collection<String> arg0) {
        // TODO Auto-generated method stub

    }

}
